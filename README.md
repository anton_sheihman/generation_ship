As a captain of a generation ship you should travel to your home planet, while maintaining resources to make it safe journey.

Download from [itch.io](https://sheix.itch.io/generation-ship)

How to play:

Click on stars (bright) to zero-jump with your ship towards earth.
While Zero-drive is gathering fuel from sun heat build(click on ship) and launch(click on planets) probes to explore planets.
Jump to the galaxy  when you have enough fuel and probes are returned to the main ship.
This game in first place was created for libGDX jam.

This game's current code is open source.

Features:

Procedural content generation
Building and launching probes to planets
Resource management
Controlling spacecraft
Stunning programmers' graphics
Weird controls

Thanks:

My very best wife who supported me all the time of the jam!

Font - https://twitter.com/codeman38 http://www.zone38.net/

Music and sounds: 

Sunlight [Naviarhaiku131 - Fall into deep space] by Flat Stone is licensed under a  Creative Commons License. 

 https://twitter.com/jalastram


