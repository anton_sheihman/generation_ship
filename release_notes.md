**v0.0.9**

- Feature: comets!
- Fix: targeting systems
- Fix: people dying on planets


**v0.0.8**

- Feature: music and sounds!


**v0.0.7**

- Feature: rescue missions!
- Feature: building probes now takes time
- Feature: hints for a galaxy screen
- Feature: accessibility: game now can be played with mouse only
- Feature: planet names generation
- Fix: now less information is hidden by screen borders
- Fix: game over screen skipped too fast
- Fix: planet generation is now in both hemispheres
- Fix: probes now lose their speed when jumping to other systems
- Balance: now game is a bit harder to win


**v0.0.6**

- Resources handling was refactored
- Balance was updated
- Wrong handling of game over condition was fixed
- Added better visuals for selecting planets
- Added better hints for actions player can do
- Fixed critical bug with resizing of the game window