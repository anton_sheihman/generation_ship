|What|file|done|
|----|----|----|
|Start game - theme music|Flat Stone - Sunlight.mp3|yes|
|Text - second theme||
|Click - theme continues, some whooshing effect | 1.vaw|yes
|Highlight star that can be jumped - sonar beep | fx391-bleep.wav | Yes
|Highlight star that can't be jumped - crack sound | fx391-bleep-wah.wav | Yes
|Jump - another whooosh| 1-low.vaw|yes
|Solar system screen - highlight - beep |fx391-bleep.wav|
|Probe launch - radio transmission |
|Probe build done | fx195-build.wav | Yes
|Probe build initiated| fx195-build-initiated.wav | Yes
|Crash - boom Landing - air releasing sound |crash.wav|yes
|Resources pickup - box dragging sound |
|Delegation - chatter |chatter-low-darkbreath.wav
|Need to build probes - crack sound |
|Not enough metals - crack sound |
|Crew dying - alarm sound| 
|Reached destination - victory trumpet|
|Game over - minor sound| 
|Event|chord_heat_default_event.wav|yes