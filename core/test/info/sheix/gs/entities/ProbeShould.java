package info.sheix.gs.entities;

import info.sheix.gs.Outcome;
import info.sheix.gs.util.Singleton;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static info.sheix.gs.entities.Probe.probeStartingSpeed;
import static info.sheix.gs.entities.TestHelper.epsilon;
import static org.junit.Assert.*;

public class ProbeShould {

    private Starship starship;

    @Before
    public void setup() {
        starship = new Starship(false);
    }

    @Test
    public void BeBuiltWithNoTarget() {
        Probe p = new Probe(starship, false);

        assertNull(p.getTarget());
    }


    @Test
    public void LaunchProbe() {
        Probe p = new Probe(starship, false);
        Probe launchProbe = launchProbe(p, createDummyTarget(10, 0.2f));

        assertNotNull(launchProbe);
        assertEquals(p, launchProbe);

    }

    @Test
    public void ShouldHaveSpeedEqualsToStarshipSpeedWhenLaunched() {
        Probe p = new Probe(starship, false);

        launchProbe(p, createDummyTarget(10, 0.2f));

        p.setTarget(createDummyTarget(10, 0.2f));

        p.makeTurn();

        float angularSpeed = p.getAngularSpeed();
        assertEquals(probeStartingSpeed, angularSpeed, epsilon);
    }

    @Test
    public void ShouldIncreaseItsSpeedNextTurnAfterLaunchLaunched() {
        Singleton.INSTANCE.debug = true;
        Probe p = new Probe(starship, false);

        launchProbe(p, createDummyTarget(10, 0.2f));

        p.setTarget(createDummyTarget(10, 0.2f));

        p.makeTurn();

        float speed = p.getAngularSpeed();

        p.makeTurn();

        float speed2 = p.getAngularSpeed();
        assertNotEquals(speed, speed2, epsilon);
    }

    @Test
    public void ShouldIncreaseSpeedAtAStartAndDecreaseWhenCloserToTarget() {
        Singleton.INSTANCE.debug = false;
        createProbeAndLaunchToTargetOnDistance(10, 0.2f);
        createProbeAndLaunchToTargetOnDistance(100, 3.1f);
        createProbeAndLaunchToTargetOnDistance(200, -1.4f);

    }

    private void createProbeAndLaunchToTargetOnDistance(int distance, float theta) {
        Probe p = new Probe(starship, false);

        OrbitalObject dummyTarget = createDummyTarget(distance, theta);
        launchProbe(p, dummyTarget);

        List<DataPoint> dataPointList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            //DataPoint dp = new DataPoint(i, p.distance - dummyTarget.distance, p.distance, p.gravitySpeed, p.gravityAcceleration);
            DataPoint dp = new DataPoint(i, p.angularDistance(dummyTarget), p.getTheta(), p.angularSpeed, p.thetaAcceleration);
            dataPointList.add(dp);
            p.makeTurn();

        }
        System.out.println("Initial target distance: " + distance + " initial theta: " + theta);
        System.out.println("turn, probeTargetDistance, probeDistance, probeSpeed, probeAcceleration");
        dataPointList.forEach(DataPoint::print);
        System.out.println("*_*_*_*_*_*_*_*_*_*_*");
    }

    class DataPoint {
        public DataPoint(int turn, double probeTargetDistance, float probeDistance, float probeSpeed, float probeAcceleration) {
            this.turn = turn;
            this.probeTargetDistance = probeTargetDistance;
            this.probeDistance = probeDistance;
            this.probeSpeed = probeSpeed;
            this.probeAcceleration = probeAcceleration;
        }

        int turn;

        double probeTargetDistance;

        float probeDistance;
        float probeSpeed;
        float probeAcceleration;

        public void print() {
            System.out.println(turn + ", " + probeTargetDistance + ", " + probeDistance + ", " + probeSpeed + ", " + probeAcceleration);
        }
    }

    private Probe launchProbe(Probe p, OrbitalObject target) {
        p.setBuilding(false);
        p.setOnBoard(true);
        starship.setProbes(Collections.singletonList(p));
        return starship.launchProbe(target);
    }

    private OrbitalObject createDummyTarget(final int distance, final float theta) {
        return new OrbitalObject(distance, 0.5f, theta) {
            @Override
            public String getInfo() {
                return this.toString();
            }

            @Override
            public void processOutcome(Outcome o) {

            }
        };
    }
}
