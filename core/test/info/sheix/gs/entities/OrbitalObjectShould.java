package info.sheix.gs.entities;

import info.sheix.gs.Outcome;
import org.junit.Test;

import static info.sheix.gs.entities.TestHelper.epsilon;
import static org.junit.Assert.*;

public class OrbitalObjectShould {

    private float PiByTwo = 3.14f / 2;

    @Test
    public void angularDistanceToObjectAtQuarterPiShouldReturnQuarterPi() {
        OrbitalObject oo = createDummyOO();
        OrbitalObject target = createDummyOO();
        oo.setTheta(0);
        target.setTheta(PiByTwo);

        double distance = oo.angularDistance(target);

        assertEquals(-PiByTwo, distance, epsilon);
    }

    @Test
    public void angularDistanceToObjectAt3QuarterPiShouldReturn3QuarterPi() {
        OrbitalObject oo = createDummyOO();
        OrbitalObject target = createDummyOO();
        oo.setTheta(0);
        target.setTheta(PiByTwo * 3);

        double distance = oo.angularDistance(target);

        assertEquals(PiByTwo, distance, epsilon);
    }

    @Test
    public void fromPiByTwoTo3halvesOfPiWithRightDirection() {
        OrbitalObject oo = createDummyOO();
        OrbitalObject target = createDummyOO();
        oo.setTheta(PiByTwo + 0.1f);
        target.setTheta(PiByTwo * 3);

        double distance = oo.angularDistance(target);

        assertEquals(-(3.14 - 0.1f), distance, epsilon);
    }

    @Test
    public void from3halvesOfPiToPiByTwoToWithRightDirection() {
        OrbitalObject oo = createDummyOO();
        OrbitalObject target = createDummyOO();
        oo.setTheta(PiByTwo * 3);
        target.setTheta(PiByTwo + 0.1f);

        double distance = oo.angularDistance(target);

        assertEquals(3.14 - 0.1f, distance, epsilon);
    }

    private OrbitalObject createDummyOO() {
        return new OrbitalObject() {
            @Override
            public void processOutcome(Outcome o) {

            }

            @Override
            public String getInfo() {
                return null;
            }
        };
    }
}