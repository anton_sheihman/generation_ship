package info.sheix.gs.util;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import info.sheix.gs.util.MessageFormatter;

public class MessageFormatterShould {

	@Test
	public void getStringAndWidthAndReturnStringList() {
		MessageFormatter sf = new MessageFormatter();
		List<String> result = sf.format("Any String", 33);
		assertNotNull(result);
	}

	@Test
	public void ifStringLengthLessThanWidthReturnItAsOnlyListElement() {
		String testShortString = "Any String";
		MessageFormatter sf = new MessageFormatter();
		List<String> result = sf.format(testShortString, 33);
		assertEquals(result.get(0), testShortString);
		assertEquals(result.size(), 1);
	}

	@Test
	public void ifStringLengthMoreThanWidthSplitTheString() {
		String testLongMessage = "Planet is dull and empty. Nothing of interest found";
		MessageFormatter mf = new MessageFormatter();
		List<String> result = mf.format(testLongMessage, 15);
		assertTrue(result.size() > 1);
	}

	@Test
	public void splitLongStringsAnyWay() {
		String testVeryLongMessage = "AbraKadabraFocusPocus";
		MessageFormatter mf = new MessageFormatter();
		List<String> result = mf.format(testVeryLongMessage, 11);
		assertTrue(result.size() == 2);
		assertEquals(result.get(0), "AbraKadabra");
		assertEquals(result.get(1), "FocusPocus");
	}
	
	@Test public void handleTwoSpacesInRow(){
	String testShortString = "Any  String";
	MessageFormatter sf = new MessageFormatter();
	List<String> result = sf.format(testShortString, 33);
	assertEquals(result.get(0), "Any String");
	assertEquals(result.size(), 1);
	}
}
