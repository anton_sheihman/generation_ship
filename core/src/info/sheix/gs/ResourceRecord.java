package info.sheix.gs;

import info.sheix.gs.entities.ResourceHolder;
import info.sheix.gs.outcomes.TurnOutcome;
import info.sheix.gs.util.Messages;

public class ResourceRecord {
    public int food;
    public int crew;
    public int metal;

    public ResourceRecord() {
    }

    public ResourceRecord(int food, int crew, int metal) {

        this.food = food;
        this.crew = crew;
        this.metal = metal;
    }

    public void add(ResourceRecord data) {
        food += data.food;
        crew += data.crew;
        metal += data.metal;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        if (food != 0) sb.append("Food: " + food + "\n");
        if (crew != 0) sb.append("Crew: " + crew + "\n");
        if (metal != 0) sb.append("Metal: " + metal + "\n");
        return sb.toString();


    }

    public ResourceRecord remove(ResourceRecord data) {
        ResourceRecord result = new ResourceRecord();
        result.food += Math.min(food, data.food);
        result.crew += Math.min(crew, data.crew);
        result.metal += Math.min(metal, data.metal);
        food -= data.food;
        if (food < 0) food = 0;
        crew -= data.crew;
        if (crew < 0) crew = 0;
        metal -= data.metal;
        if (metal < 0) metal = 0;
        return result;
    }

    public void move(ResourceHolder from, ResourceHolder to) {
        ResourceRecord suceeded = from.getResourceHolder().remove(this);
        to.getResourceHolder().add(suceeded);
    }

    public TurnOutcome makeTurn() {
        TurnOutcome result = new TurnOutcome();
        food -= crew;
        if (food < 0)
            food = 0;
        if (crew < 0)
            crew = 0;

        return result;
    }
}