/*
 * Copyright (c) 2017, Omnisol Ltd. All rights reserved.
 * OMNISOL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package info.sheix.gs;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import info.sheix.gs.galaxy.GalaxyRenderer;
import info.sheix.gs.util.Singleton;

public class StartProcessor extends GalaxyRenderer {
    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        super.keyUp(keycode);
        restartGame();
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        restartGame();
        return false;
    }

    private void restartGame() {
        gameState.init(generator.generateGalaxy());
        gameState.setGameMode(GameMode.GALAXY);
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }

    @Override
    public void render(Batch batch, GameState gameState) {
        super.render(batch, gameState);
        BitmapFont font = Singleton.INSTANCE.getAssetManager().get("fonts/kongtext.fnt");
        font.setColor(Color.LIGHT_GRAY);
        StringBuilder sb = new StringBuilder();
        sb.append("Long long time ago, \nin a galaxy far far away");
        sb.append("\n\n");
        sb.append("Oh no, that's another story...\n\n");
        sb.append("Awaken from a cryogenic sleep on huge\ngeneration ship\n");
        sb.append("By a distress call from the home star\n");
        sb.append("Finding a way through stars to... stars\n");
        sb.append("With a crew that ready for anything\n");
        sb.append("\n\n");
        sb.append("Good luck!");

        font.draw(batch, sb.toString(), 15, 400);

    }
}
