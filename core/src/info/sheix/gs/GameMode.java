package info.sheix.gs;

public enum GameMode {
    GALAXY, SOLAR_SYSTEM, START, GAME_OVER, LOGO
}
