package info.sheix.gs;

import com.badlogic.gdx.math.Vector2;

import info.sheix.gs.entities.Destination;
import info.sheix.gs.entities.SolarSystem;

import java.util.Arrays;

public class Galaxy {

    private SolarSystem[][] galaxyMap = new SolarSystem[Constants.galaxySizeX][Constants.galaxySizeY];
    private boolean firstRun = true;
    private SolarSystem destination;

    public void setSolarSystem(int i, int j, SolarSystem system) {
        if (firstRun) {
            for (int k = 0; k < Constants.galaxySizeX; k++) {
                galaxyMap[k] = new SolarSystem[Constants.galaxySizeY];

            }
            firstRun = false;
        }

        galaxyMap[i][j] = system;
        if (system instanceof Destination)
            this.setDestination(system);
    }

    public SolarSystem getSolarSystem(int i, int j) {
        return galaxyMap[i][j];
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < Constants.galaxySizeX; i++) {
            for (int j = 0; j < Constants.galaxySizeY; j++) {
                if (!galaxyMap[i][j].equals(SolarSystem.noSolarSystem)) {
                    sb.append(galaxyMap[i][j].toString()).append(" [ ").append(i).append(", ").append(j).append("] \n");

                }
            }
        }

        return sb.toString();
    }

    public void updateVisibles(Vector2 location, int r) {
        int X = (int) location.x;
        int Y = (int) location.y;

        Arrays.stream(galaxyMap).forEach(ssArray -> Arrays.stream(ssArray).forEach(system -> {
                    if (system != null) system.setVisible(false);
                })
        );

        for (int x = -r; x <= r; x++) {
            for (int y = -r; y <= r; y++) {
                if ((X + x < 0) || (X + x >= Constants.galaxySizeX) || (Y + y < 0) || (Y + y >= Constants.galaxySizeY))
                    continue;
                if ((x * x) + (y * y) < r * r) {
                    if (galaxyMap[X + x][Y + y] != null) {
                        galaxyMap[X + x][Y + y].setVisible(true);
                    }
                }
            }
        }

    }


    private void setDestination(SolarSystem destination) {
        this.destination = destination;
    }

    SolarSystem getDestination() {
        return destination;
    }
}
