/*
 * Copyright (c) 2017, Omnisol Ltd. All rights reserved.
 * OMNISOL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package info.sheix.gs;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.math.Vector2;
import info.sheix.gs.entities.SolarSystem;
import info.sheix.gs.galaxy.GalaxyRenderer;
import info.sheix.gs.util.Singleton;
import info.sheix.gs.util.SoundManager;
import info.sheix.gs.util.Sounds;
import info.sheix.gs.util.TimeUtils;

public class LogoProcessor extends GalaxyRenderer {
    private final Galaxy logoGalaxy;

    public LogoProcessor(Galaxy logoGalaxy) {
        this.logoGalaxy = logoGalaxy;
    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        super.keyUp(keycode);
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        if (gameState == null)
            return false;
        gameState.setGameMode(GameMode.START);
        SoundManager.play(Sounds.StartGame);
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }

    @Override
    public void render(Batch batch, GameState gameState) {
        super.render(batch, gameState);
        BitmapFont font = Singleton.INSTANCE.getAssetManager().get("fonts/kongtext.fnt");
        font.setColor(Color.LIGHT_GRAY);
        StringBuilder sb = new StringBuilder();

        sb.append("GENERATION\n      SHIP");
        sb.append("\n\n\n\n");
        if (TimeUtils.shouldBlink()) {
            sb.append("PRESS ANY KEY TO START");
        }
        sb.append("\n\n");

        sb.append("<M> music <S> sound");

        if (Singleton.INSTANCE.debug) {
            sb.append("\n\n");

            sb.append("Cheat mode on!");
        }
        font.draw(batch, sb.toString(), 15, 400);

        renderGalaxyForLogo(batch);
    }

    private void renderGalaxyForLogo(Batch batch) {
        for (int i = 0; i < Constants.galaxySizeX; i++) {
            for (int j = 0; j < Constants.galaxySizeY; j++) {
                boolean highlighted = false;

                if (Singleton.INSTANCE.getRandom().nextInt(200) == 0) {
                    highlighted = true;
                }
                renderGalaxySpotForLogo(batch, getScreenPosition(i, j), logoGalaxy.getSolarSystem(i, j), highlighted);
            }
        }
    }

    private void renderGalaxySpotForLogo(Batch batch, Vector2 v, SolarSystem solarSystem2, boolean highlighted) {
        if (solarSystem2 == null) {
            return;
        }
        if (solarSystem2.getSprite() == null) {
            return;
        }
        solarSystem2.getSprite().setPosition(v.x, v.y);

        if (Singleton.INSTANCE.getRandom().nextInt(200) == 0)
            solarSystem2.getSprite().scale(0.5f - Singleton.INSTANCE.getRandom().nextFloat());

        if (highlighted) {
            solarSystem2.getSprite().setRotation(Singleton.INSTANCE.getRandom().nextInt(90));
        }

        solarSystem2.getSprite().setColor(solarSystem2.getStarColor());
        solarSystem2.getSprite().setAlpha(solarSystem2.getAlpha());
        solarSystem2.getSprite().draw(batch);

    }
}
