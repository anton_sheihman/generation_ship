package info.sheix.gs.outcomes;

import info.sheix.gs.Outcome;

public class NothingHappenedOutcome extends Outcome {

	public NothingHappenedOutcome() {
		super();
		message = "The planet is dull and empty. Nothing found.";
	}

}
