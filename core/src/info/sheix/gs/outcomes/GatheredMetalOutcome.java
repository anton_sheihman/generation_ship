package info.sheix.gs.outcomes;

import info.sheix.gs.Outcome;

public class GatheredMetalOutcome extends Outcome {
	public GatheredMetalOutcome(){
		super();
		message ="Unique atmosphere and soil created useful natural alloys on this planet.";
		data.metal += 50;
	}

}
