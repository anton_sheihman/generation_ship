package info.sheix.gs.outcomes;

import info.sheix.gs.Outcome;

public class GatheredFoodOutcome extends Outcome {
	public GatheredFoodOutcome() {
		super();
		data.food += 50;
		message = "The planet have vivid plant and animal life. Your crew gathers some edible fruit.";
	}
}
