package info.sheix.gs.outcomes;

import info.sheix.gs.Outcome;

public class LandedSuccessfullyOutcome extends Outcome {
	public LandedSuccessfullyOutcome(){
		super();
		message ="Crew have landed probe on this planet, but failed to find useful quantities of resources";
		data.metal += 10;

	}
}
