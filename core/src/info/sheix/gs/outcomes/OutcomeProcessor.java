package info.sheix.gs.outcomes;

import info.sheix.gs.Outcome;
import info.sheix.gs.ResourceRecord;
import info.sheix.gs.entities.Comet;
import info.sheix.gs.entities.OrbitalObject;
import info.sheix.gs.entities.Planet;
import info.sheix.gs.entities.Probe;
import info.sheix.gs.util.Messages;
import info.sheix.gs.util.Singleton;

import java.util.List;

public class OutcomeProcessor {

    public Outcome processOutcome(OrbitalObject oo, Probe probe) {
        if (oo instanceof Planet) {
            Planet planet = (Planet) oo;
            if (probe.getTarget() == null)
                oo.processOutcome(new CrashOutcome(probe.getResourceHolder()));

            if (!probe.getTarget().equals(oo))
                return new NoOutcome();

            List<Outcome> possibleOutcomes = planet.getPossibleOutcomes(probe);
            if (Singleton.INSTANCE.debug)
                System.out.println("Possible outcomes: " + possibleOutcomes.size());
            Outcome o = possibleOutcomes.get(Singleton.INSTANCE.getRandom().nextInt(possibleOutcomes.size()));

            oo.processOutcome(o);
            probe.processOutcome(o);

            return o;
        } else {
            if (oo instanceof Comet) {
                if (Singleton.INSTANCE.getRandom().nextInt(10) == 0) {
                    return new AddedCrewOutcome() {
                        @Override
                        public String getMessage() {
                            return Messages.COMET_MAGIC_OUTCOME;
                        }

                    };
                }
                return new CrashOutcome(new ResourceRecord());
            }
        }
        return new NoOutcome();
    }

    private class NoOutcome extends Outcome {
    }
}
