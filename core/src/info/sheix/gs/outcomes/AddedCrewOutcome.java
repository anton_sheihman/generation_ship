package info.sheix.gs.outcomes;

import info.sheix.gs.Outcome;
import info.sheix.gs.ResourceRecord;
import info.sheix.gs.util.Messages;

public class AddedCrewOutcome extends Outcome {
	public AddedCrewOutcome() {
		super();
		data = new ResourceRecord();
		data.crew += 5;
		data.food += 30;
	}

	@Override
	public String getMessage() {
		return Messages.ADDED_CREW_OUTCOME;
	}
}
