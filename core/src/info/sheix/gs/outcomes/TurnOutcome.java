/*
 * Copyright (c) 2018, Omnisol Ltd. All rights reserved.
 * OMNISOL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package info.sheix.gs.outcomes;

import info.sheix.gs.Effects;
import info.sheix.gs.Outcome;

public class TurnOutcome extends Outcome{
    private Effects effect;

    public TurnOutcome(String message) {
        this.message = message;
    }

    public TurnOutcome() {

    }

    public TurnOutcome(String message, Effects effects) {
        this.effect = effects;
        this.message = message;
    }

    public void setEffect(Effects effect) {
        this.effect = effect;
    }

    public Effects getEffect() {
        return effect;
    }
}
