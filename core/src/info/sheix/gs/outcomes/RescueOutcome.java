package info.sheix.gs.outcomes;

import info.sheix.gs.Outcome;
import info.sheix.gs.ResourceRecord;

public class RescueOutcome extends Outcome {
    public RescueOutcome(ResourceRecord data) {
        this.data = data;
        setMessage("The rescue mission succeeded!");
    }


}
