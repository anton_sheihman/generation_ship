package info.sheix.gs.outcomes;

import info.sheix.gs.Outcome;
import info.sheix.gs.ResourceRecord;

public class CrashOutcome extends Outcome {

    public CrashOutcome(ResourceRecord data) {
        super();
        this.data = data;
        message = "Probe crashed.";
    }
}
