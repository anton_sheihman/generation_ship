/*
 * Copyright (c) 2017, Omnisol Ltd. All rights reserved.
 * OMNISOL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package info.sheix.gs.galaxy;

import com.badlogic.gdx.math.Vector2;
import info.sheix.gs.entities.generators.GalaxyGenerator;
import info.sheix.gs.interfaces.Renderer;

import static info.sheix.gs.Constants.*;

public abstract class GalaxyRenderer extends Renderer {

    protected GalaxyGenerator generator = new GalaxyGenerator();

    protected Vector2 getScreenPosition(Vector2 location) {
        return getScreenPosition((int) location.x, (int) location.y);

    }

    protected Vector2 getScreenPosition(int x, int y) {
        float marginY = gameY * 0.2f;
        float marginX = gameX * 0.4f;

        float startX = marginX / 2f;
        float startY = marginY / 2f;

        float xn = (gameX - marginX) / galaxySizeX;
        float yn = (gameY - marginY) / galaxySizeY;

        float actualX = startX + xn * (x);
        float actualY = startY + yn * (y);
        return new Vector2(actualX, actualY);
    }

}
