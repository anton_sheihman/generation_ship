/*
 * Copyright (c) 2016, Omnisol Ltd. All rights reserved.
 * OMNISOL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package info.sheix.gs.galaxy;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector2;
import info.sheix.gs.Constants;
import info.sheix.gs.GameMode;
import info.sheix.gs.GameState;
import info.sheix.gs.Origin;
import info.sheix.gs.entities.Probe;
import info.sheix.gs.entities.SolarSystem;
import info.sheix.gs.util.Sounds;
import info.sheix.gs.util.SoundManager;

import static info.sheix.gs.Constants.gameX;
import static info.sheix.gs.Constants.gameY;

public class GalaxyStateProcessor extends GalaxyRenderer {

    private int highlightedY;
    private int highlightedX;
    private boolean firstTime;

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        super.keyUp(keycode);
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        if (sourceEqualsDestination()) {
            gameState.setGameMode(GameMode.SOLAR_SYSTEM);
            return false;
        }

        if (gameState.getStarshipGalaxy().getLocation().dst(highlightedX, highlightedY) < 1f) {
            gameState.setGameMode(GameMode.SOLAR_SYSTEM);
            return false;
        }

        if (gameState.getStarship().haveEnoughFuelToJump() && highlightedSolarSystemIsNotEmpty() && destinationInVisibilityRadius()) {
            zeroJump(highlightedX, highlightedY);
        }


        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }

    private Vector2 getMapPosition(int actualX, int actualY) {
        double marginY = gameY * 0.2;
        double marginX = gameX * 0.4;

        double startX = marginX / 2;
        double startY = marginY / 2;

        double xn = (gameX - marginX) / Constants.galaxySizeX;
        double yn = (gameY - marginY) / Constants.galaxySizeY;

        double positionX = (actualX - startX) / xn;
        double positionY = Constants.galaxySizeY - (actualY - startY) / yn;

        int x = (int) (positionX);
        int y = (int) (positionY);
        return new Vector2(x, y);

    }

    @Override
    public void render(Batch batch, GameState gameState) {
        super.render(batch, gameState);

        Vector2 mapVector = getMapPosition(x, y);

        renderGalaxyStarShip(batch);

        for (int i = 0; i < Constants.galaxySizeX; i++) {
            for (int j = 0; j < Constants.galaxySizeY; j++) {
                boolean highlighted = false;

                if (i == (int) mapVector.x && j == (int) mapVector.y) {
                    highlighted = true;
                    highlightedX = (int) mapVector.x;
                    highlightedY = (int) mapVector.y;

                }
                renderGalaxySpot(getScreenPosition(i, j), gameState.getGalaxy().getSolarSystem(i, j), highlighted, batch);
            }
        }
        if (highlightedSolarSystemIsNotEmpty()) {
            if (sourceEqualsDestination())
                addHint("Click to enter star system!");
            else
                addHint("Click to jump to the selected star!");
            if (firstTime) {
                if (destinationInVisibilityRadius())
                    SoundManager.play(Sounds.SelectStar);
                else
                    SoundManager.play(Sounds.SelectStarNotInRadius);
                firstTime = false;
            }

        } else {
            addHint("Click on a highlighted star to jump");
            firstTime = true;
        }

        if (!gameState.getStarship().haveEnoughFuelToJump()) {
            addHint("You do not have enough zero-fuel.\nSpend more turns near the star");
        }

        renderHints(batch);
    }


    private void renderGalaxyStarShip(Batch batch) {
        renderStats(batch);
        Vector2 v = getScreenPosition(gameState.getStarshipGalaxy().getLocation());
        gameState.getStarshipGalaxy().getSprite().setPosition(v.x, v.y + 16);
        gameState.getStarshipGalaxy().getSprite().setOrigin(8, -8);
        gameState.getStarshipGalaxy().getSprite().rotate(2f);
        gameState.getStarshipGalaxy().getSprite().draw(batch);
    }


    private void renderGalaxySpot(Vector2 v, SolarSystem solarSystem2, boolean highlighted, Batch batch) {
        if (solarSystem2 == null) {
            return;
        }
        if (solarSystem2.getSprite() == null) {
            return;
        }
        solarSystem2.draw(batch, v.x, v.y, Origin.SPRITE_CENTER, highlighted);
    }

    private void zeroJump(int i, int j) {
        for (Probe probe : gameState.getStarship().getProbes()) {
            if (!probe.isOnBoard()) {
                probe.kill();
            }
        }
        gameState.getStarshipGalaxy().setLocation(new Vector2(i, j));
        gameState.getStarship().setZeroDriveFuel(0);
        gameState.getGalaxy().updateVisibles(gameState.getStarshipGalaxy().getLocation(), gameState.getStarshipGalaxy().getVisibilityRadius());
        SoundManager.play(Sounds.Jump);
        gameState.setSolarSystem(gameState.getGalaxy().getSolarSystem(i, j));
    }

    private boolean destinationInVisibilityRadius() {
        float distance = gameState.getStarshipGalaxy().getLocation().dst(highlightedX, highlightedY);
        float radius = gameState.getStarshipGalaxy().getVisibilityRadius();
        return distance < radius;
    }

    private boolean highlightedSolarSystemIsNotEmpty() {
        return !gameState.getGalaxy().getSolarSystem(highlightedX, highlightedY).equals(SolarSystem.noSolarSystem);
    }

    private boolean sourceEqualsDestination() {
        return new Vector2(highlightedX, highlightedY).equals(gameState.getStarshipGalaxy().getLocation());
    }


}
