/*
 * Copyright (c) 2018, Omnisol Ltd. All rights reserved.
 * OMNISOL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package info.sheix.gs;

public enum Effects {
    CREW_MEMBER_DIED,
    PROBE_EXPLODED
}
