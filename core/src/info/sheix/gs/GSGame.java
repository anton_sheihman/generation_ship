package info.sheix.gs;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import info.sheix.gs.entities.generators.GalaxyGenerator;
import info.sheix.gs.galaxy.GalaxyStateProcessor;
import info.sheix.gs.interfaces.Renderer;
import info.sheix.gs.util.Singleton;
import info.sheix.gs.util.SoundManager;
import info.sheix.gs.util.Sounds;

import java.io.IOException;

import static java.nio.file.Files.readAllBytes;
import static java.nio.file.Paths.get;

public class GSGame extends ApplicationAdapter implements ApplicationListener {

    private SpriteBatch batch;

    private Camera camera;

    private GalaxyGenerator galaxyGenerator;

    private GameState gameState;
    private Renderer gameOverProcessor;
    private Renderer startProcessor;
    private Renderer galaxyProcessor;
    private Renderer logoProcessor;
    private Renderer solarSystemProcessor;

    public GSGame() {
    }

    @Override
    public void create() {
        String version;
        try {
            version = new String(readAllBytes(get("VERSION")));
        } catch (IOException e) {
            System.out.println("Cant load file, using internal:");
            FileHandle file = Gdx.files.internal("VERSION");
            version = file.readString();
        }
        if (version.contains("DEBUG"))
            Singleton.INSTANCE.debug = true;
        System.out.println("Generation Ship " + version);
        batch = new SpriteBatch();

        camera = new OrthographicCamera(640, 480);

        loadAssets();
        galaxyGenerator = new GalaxyGenerator();

        gameState = new GameState(galaxyGenerator.generateGalaxy());
        startProcessor = new StartProcessor();
        gameOverProcessor = new GameOverProcessor();
        galaxyProcessor = new GalaxyStateProcessor();
        logoProcessor = new LogoProcessor(galaxyGenerator.generateGalaxy());
        solarSystemProcessor = new SolarSystemProcessor();


        Gdx.input.setInputProcessor(logoProcessor);
        SoundManager.playTheme();

    }

    private void loadAssets() {
        AssetManager assetManager = Singleton.INSTANCE.getAssetManager();
        assetManager.load("fonts/kongtext.fnt", BitmapFont.class);
        assetManager.load("star.png", Texture.class);
        assetManager.load("ship.png", Texture.class);
        assetManager.load("earth.png", Texture.class);
        assetManager.load("probe.png", Texture.class);
        assetManager.load("4.png", Texture.class);
        assetManager.load("mark_v.png", Texture.class);
        assetManager.load("sos.png", Texture.class);
        assetManager.load("crosshair.png", Texture.class);

        assetManager.load("explosionEmitter.pe", ParticleEffect.class);
        assetManager.load("soulEmitter.pe", ParticleEffect.class);
        assetManager.load("cometEmitter.pe", ParticleEffect.class);


        for (int i = 1; i < 13; i++) {
            assetManager.load("planet" + i + ".png", Texture.class);
        }

        for (Sounds sounds : Sounds.values()) {
            assetManager.load(sounds.getFileName(), Sound.class);
        }

        while (!assetManager.update()) {
            /*
            System.out.println("Loaded: " + assetManager.getProgress() * 100 +
            "%");
            */
        }
    }

    @Override
    public void render() {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        renderScreen();
    }

    private void renderScreen() {
        batch.begin();
        Renderer renderer = getRenderer(gameState.getGameMode());
        Gdx.input.setInputProcessor(renderer);

        if (renderer != null) {
            renderer.render(batch, gameState);
        }

        batch.end();
    }

    private Renderer getRenderer(GameMode gameMode) {
        switch (gameMode) {
            case GAME_OVER:
                return gameOverProcessor;
            case GALAXY:
                return galaxyProcessor;
            case LOGO:
                return logoProcessor;
            case SOLAR_SYSTEM:
                return solarSystemProcessor;
            case START:
                return startProcessor;
            default:
        }
        return null;
    }


    @Override
    public void resize(int width, int height) {
        Singleton.INSTANCE.X = width;
        Singleton.INSTANCE.Y = height;
        camera.viewportHeight = height;
        camera.viewportWidth = width;
    }


}
