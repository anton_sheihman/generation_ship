/*
 * Copyright (c) 2016, Omnisol Ltd. All rights reserved.
 * OMNISOL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package info.sheix.gs.interfaces;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import info.sheix.gs.GameState;
import info.sheix.gs.util.MessageFormatter;
import info.sheix.gs.util.Messages;
import info.sheix.gs.util.Singleton;
import info.sheix.gs.util.SoundManager;

import java.util.*;
import java.util.stream.Collectors;

import static info.sheix.gs.util.TimeUtils.shouldBlink;

public abstract class Renderer implements InputProcessor {
    protected GameState gameState;
    protected Queue<String> messages = new LinkedList<>();

    protected int x;
    protected int y;
    protected List<String> hints;
    private BitmapFont font;

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        x = screenX;
        y = screenY;
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        if (keycode == Input.Keys.M)
            SoundManager.muteMusic();
        if (keycode == Input.Keys.S)
            SoundManager.muteSound();
        return true;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        if (haveMessages()) {
            messages.remove();
            return true;
        }
        return false;
    }

    protected boolean haveMessages() {
        return messages.size() != 0;
    }

    protected void renderMessage(Batch batch) {
        if (!haveMessages()) return;

        String message = messages.peek();

        BitmapFont font = Singleton.INSTANCE.getAssetManager().get("fonts/kongtext.fnt");
        font.setColor(Color.LIGHT_GRAY);
        int messagesFormattedSize = 0;

        if (!(message == null || message.length() == 0)) {

            List<String> messagesFormatted = MessageFormatter.format(message, 33);
            Collections.reverse(messagesFormatted);
            messagesFormattedSize = messagesFormatted.size();
            for (int i = 0; i < messagesFormattedSize; i++) {
                font.draw(batch, messagesFormatted.get(i), 50, i * 16 + 100);
            }
        }
        if (messages.size() > 1 && shouldBlink())
            font.draw(batch, "more...", 50, messagesFormattedSize * 16 + 100);

    }

    public void render(Batch batch, GameState gameState) {
        hints = new ArrayList<>();
        if (haveMessages())
            addHint(Messages.ACKNOWLEDGE);
        renderMessage(batch);
        this.gameState = gameState;
    }

    protected void renderStats(Batch batch) {
        if (font == null) {
            font = Singleton.INSTANCE.getAssetManager().get("fonts/kongtext.fnt");
            font.setColor(Color.LIGHT_GRAY);
        }
        StringBuilder statisticsString = new StringBuilder();
        statisticsString.append("0f:[");
        statisticsString.append(gameState.getStarship().getZeroDriveFuel());
        statisticsString.append("/");
        statisticsString.append(gameState.getStarship().getZeroDriveFuelToJump());
        statisticsString.append("] Probes: [");
        for (int i = 0; i < gameState.getStarship().maxprobes; i++) {
            try {
                statisticsString.append(gameState.getStarship().getProbes().get(i).getChar());
            } catch (Exception e) {
                statisticsString.append("0");
            }
        }

        statisticsString.append("]\n");

        statisticsString.append("Crew:");
        statisticsString.append(gameState.getStarship().getCrew());

        statisticsString.append(" Met:");
        statisticsString.append(gameState.getStarship().getMetal());

        statisticsString.append(" Food: ");
        statisticsString.append(gameState.getStarship().getFood());

        font.draw(batch, statisticsString.toString(), 10, 470);

    }

    protected void renderHints(Batch batch) {
        BitmapFont font = Singleton.INSTANCE.getAssetManager().get("fonts/kongtext.fnt");
        font.setColor(Color.LIGHT_GRAY);
        String collect = hints.stream().collect(Collectors.joining("\n"));
        font.draw(batch, collect, 10, 430);
    }

    protected void addHint(String hint) {
        if (hints.contains(Messages.ACKNOWLEDGE))
            return;
        hints.add(hint);
    }
}
