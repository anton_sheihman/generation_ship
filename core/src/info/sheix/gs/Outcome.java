package info.sheix.gs;

public abstract class Outcome {

    public Outcome() {
        data = new ResourceRecord();
    }

    public ResourceRecord data;
    protected String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
