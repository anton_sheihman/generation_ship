package info.sheix.gs.util;

import com.badlogic.gdx.assets.AssetManager;
import info.sheix.gs.ParticleManager;
import info.sheix.gs.util.name_generator.NameGenerator;
import info.sheix.gs.util.name_generator.StringGenerator;
import info.sheix.gs.util.name_generator.implementation.MarkovChainsGenerator;
import info.sheix.gs.util.name_generator.implementation.MarkovContainer;
import info.sheix.markov.CharacterChain;

import java.util.Calendar;
import java.util.Random;

/**
 * Created by sheix on 02/09/16.
 */
public enum Singleton {
    INSTANCE {

    };

    private final MarkovContainer markovContainer;
    private MarkovChainsGenerator planetsGenerator;
    private MarkovChainsGenerator probesGenerator;
    public ParticleManager particleManager;
    public boolean debug;

    public MarkovChainsGenerator getProbesGenerator() {
        return probesGenerator;
    }


    Singleton() {
        markovContainer = new MarkovContainer();
        particleManager = new ParticleManager();
        try {
            planetsGenerator = new MarkovChainsGenerator(markovContainer.getChain("planets.txt"));
            probesGenerator = new MarkovChainsGenerator(markovContainer.getChain("probes.txt"));
        } catch (Exception e) {
            planetsGenerator = new MarkovChainsGenerator(new CharacterChain());
            probesGenerator = new MarkovChainsGenerator(new CharacterChain());
        }
    }

    private NameGenerator nameGenerator;
    private AssetManager assetManager;

    public NameGenerator getNameGenerator() {
        if (nameGenerator == null)
            nameGenerator = new NameGenerator();
        return nameGenerator;
    }

    public AssetManager getAssetManager() {
        if (assetManager == null)
            assetManager = new AssetManager();
        return assetManager;
    }


    private Random r;

    public Random getRandom() {
        if (r == null)
            r = new Random(Calendar.getInstance().getTimeInMillis());
        return r;
    }

    public int X;

    public int Y; // window

    public MarkovChainsGenerator getPlanetsGenerator() {
        return planetsGenerator;
    }
}
