package info.sheix.gs.util;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;

/**
 * Created by Sheix on 8/24/2017.
 */
public class SoundManager {

    private static boolean musicMuted = false;
    private static boolean soundMuted = false;

    private static Music music;

    public static void play(Sounds sound) {
        if (!soundMuted)
            Singleton.INSTANCE.getAssetManager().get(sound.getFileName(), Sound.class).play();
    }

    public static void muteMusic() {
        musicMuted = !musicMuted;
        if (musicMuted) {
            music.stop();
        } else {
            music.play();
        }
    }

    public static void muteSound() {
        soundMuted = !soundMuted;
    }

    public static void playTheme() {
        if (music == null) {
            music = Gdx.audio.newMusic(Gdx.files.internal("sound/Flat Stone - Sunlight.mp3"));
        }
        music.setLooping(true);
        music.play();
    }
}
