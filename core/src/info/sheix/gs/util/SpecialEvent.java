package info.sheix.gs.util;

import info.sheix.gs.ResourceRecord;
import info.sheix.gs.entities.Starship;

import java.util.function.Function;

public class SpecialEvent {
    public static SpecialEvent NothingHappened;
    private String message;
    private ResourceRecord resources;
    private Function<Starship,Starship> processingFunction;

    public String getMessage() {
        return message;
    }

    public ResourceRecord getResources() {
        return resources;
    }

    public Function<Starship, Starship> getProcessingFunction() {
        return processingFunction;
    }


    public SpecialEvent(String message, ResourceRecord resources, Function<Starship, Starship> processingFunction) {

        this.message = message;
        this.resources = resources;
        this.processingFunction = processingFunction;
    }


}
