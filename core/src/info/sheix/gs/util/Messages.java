package info.sheix.gs.util;

public class Messages {

    public static final String ACKNOWLEDGE = "Click to acknowledge";
    public static final String PROBE_GOT_NEAR_SUN = "Probe got too close to the star!";
    public static final String CREW_MEMBER_DIED = "Crew member has died of hunger!";
    public static final String BUILD_SUCCEEDED = "Your engineering crew succeeded to build new probe!";


    public static final String COMET_MAGIC_OUTCOME = "You've passed safely through comets tail!";
    public static final String ADDED_CREW_OUTCOME = "Sapient species sent a delegation to help you reach your goal!";
}
