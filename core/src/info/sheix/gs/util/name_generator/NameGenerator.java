package info.sheix.gs.util.name_generator;

import java.util.ArrayList;
import java.util.List;

public class NameGenerator {

    public List<String> generateNames(NamingAlgorithm algorithm, int size) {
        List<String> output = new ArrayList<>();
        List<String> filtered = new ArrayList<>();
        while (output.size() < size) {
            while (filtered.size() < size) {
                List<String> generated = algorithm.generator().generate(size * 2);
                filtered.addAll(filter(algorithm, generated));
            }

            List<String> processed = new ArrayList<>();
            for (String string : filtered) {
                String processedString = string;
                for (StringProcessor processor : algorithm.processors()) {
                    processedString = processor.process(processedString);
                }
                processed.add(processedString);

            }
            output.addAll(processed);
        }

        return output;

    }

    private List<String> filter(NamingAlgorithm algorithm, List<String> generated) {
        List<String> filtered = new ArrayList<>();
        for (String string : generated) {
            boolean keep = true;
            for (StringFilter filter : algorithm.filters()) {
                if (!filter.keep(string))
                    keep = false;
            }
            if (keep) filtered.add(string);
        }
        return filtered;
    }

}
