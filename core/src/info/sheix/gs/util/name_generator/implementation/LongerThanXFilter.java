package info.sheix.gs.util.name_generator.implementation;

import info.sheix.gs.util.name_generator.StringFilter;

public class LongerThanXFilter implements StringFilter {
    private int length;

    public LongerThanXFilter(int length) {
        this.length = length;
    }

    @Override
    public boolean keep(String string) {
        return string.length()> length;
    }
}
