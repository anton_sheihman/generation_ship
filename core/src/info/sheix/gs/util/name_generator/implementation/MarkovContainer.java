package info.sheix.gs.util.name_generator.implementation;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import info.sheix.markov.CharacterChain;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;

public class MarkovContainer {

    private Map<String, CharacterChain> chains = new HashMap<>();

    private void add(String fileName){
        CharacterChain chain = new CharacterChain();
        try {
            chain.readCorpus(new File(fileName));
        } catch (FileNotFoundException e) {
            System.out.println("Error can't load " + fileName + "- using internal");
            FileHandle file = Gdx.files.internal(fileName);
            chain.readCorpus(file.readString());
        }

        chains.put(fileName, chain);
    }

    public CharacterChain getChain(String fileName){
        if (!chains.containsKey(fileName))
            add(fileName);
        return chains.get(fileName);
    }

}
