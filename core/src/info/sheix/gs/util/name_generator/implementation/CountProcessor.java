package info.sheix.gs.util.name_generator.implementation;

public class CountProcessor implements info.sheix.gs.util.name_generator.StringProcessor {

    private int counter;

    public CountProcessor() {
        counter = 0;
    }

    @Override
    public String process(String processedString) {
        counter++;
        return processedString + " " + counter;
    }
}
