package info.sheix.gs.util.name_generator.implementation;

import info.sheix.markov.CharacterChain;

import java.util.ArrayList;
import java.util.List;

public class MarkovChainsGenerator {

    private CharacterChain chain;

    public MarkovChainsGenerator(CharacterChain characterChain) {
        chain = characterChain;
    }


    public List<String> generateSame(int count) {
        String generated = chain.getNext();
        List<String> result = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            result.add(generated);
        }
        return result;
    }

    public List<String> generateSameWithMinLength5(int count) {
        StringBuilder generated = new StringBuilder(chain.getNext());
        while (generated.length()<5)
            generated.append(" ").append(chain.getNext());
        List<String> result = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            result.add(generated.toString());
        }
        return result;
    }

    public List<String> generateDifferent(int count) {
        List<String> result = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            String generated = chain.getNext();
            result.add(generated);
        }
        return result;
    }
}
