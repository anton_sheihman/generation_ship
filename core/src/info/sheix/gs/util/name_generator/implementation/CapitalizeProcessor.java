package info.sheix.gs.util.name_generator.implementation;

import info.sheix.gs.util.name_generator.StringProcessor;

import static org.apache.commons.lang3.text.WordUtils.capitalize;

public class CapitalizeProcessor implements StringProcessor {
    @Override
    public String process(String processedString) {
        return capitalize(processedString);
    }
}
