package info.sheix.gs.util.name_generator;

public interface StringProcessor {
    String process(String processedString);
}
