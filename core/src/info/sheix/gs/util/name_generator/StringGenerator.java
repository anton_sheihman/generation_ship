package info.sheix.gs.util.name_generator;

import java.util.List;

@FunctionalInterface
public interface StringGenerator {
    List<String> generate(int count);
}
