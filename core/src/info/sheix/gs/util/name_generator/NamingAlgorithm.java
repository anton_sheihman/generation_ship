package info.sheix.gs.util.name_generator;

import java.util.List;

public class NamingAlgorithm {

    private StringGenerator generator;
    private List<StringFilter> filters;
    private List<StringProcessor> processors;

    public NamingAlgorithm(StringGenerator generator, List<StringFilter> filters, List<StringProcessor> processors) {
        this.generator = generator;
        this.filters = filters;
        this.processors = processors;
    }

    public StringGenerator generator() {
        return generator;
    }

    public List<StringFilter> filters() {
        return this.filters;
    }

    public List<StringProcessor> processors() {
        return this.processors;
    }
}
