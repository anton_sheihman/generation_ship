package info.sheix.gs.util.name_generator;

public interface StringFilter {
    boolean keep(String string);
}
