package info.sheix.gs.util;

/**
 * Created by Sheix on 8/24/2017.
 */
public enum Sounds {
    BuildProbe("fx195-build.wav"),
    BuildProbeInitiated("fx-195-build-initiated.wav"),
    Launch("chatter-low-darkbreath.wav"),
    SelectStarNotInRadius("fx391-bleep-wah.wav"),
    SelectStar("fx391-bleep.wav"),
    Jump("1-short.wav"),

    EventOnEnteringSolarSystem("chord_heat_default_event.wav"),
    StartGame("1.wav"),
    CrashSound("crash.wav");

    public String getFileName() {
        return value;
    }

    private final String value;

    Sounds(String s) {
        value = "sound/" + s;
    }
}
