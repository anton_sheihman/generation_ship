/*
 * Copyright (c) 2017, Omnisol Ltd. All rights reserved.
 * OMNISOL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package info.sheix.gs.util;

import java.util.Calendar;

public class TimeUtils {
    public static boolean shouldBlink() {
        return Calendar.getInstance().get(Calendar.SECOND) % 2 == 0;
    }

}
