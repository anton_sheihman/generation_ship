package info.sheix.gs.util;

import java.util.ArrayList;
import java.util.List;

public class MessageFormatter {

	public static List<String> format(String message, int width) {
		List<String> result = new ArrayList<String>();
		String[] words = message.split(" ", 1000);
		StringBuilder sb = new StringBuilder();
		List<String> newWords = new ArrayList<String>();
		for (String word : words) {
			if (word.length() == 0)
				continue;
			if (word.length() < width)
				newWords.add(word);
			else {
				int multiplier = word.length() / width;
				for (int i = 0; i <= multiplier; i++)
					try {
						newWords.add(word.substring(i * width, (i + 1) * width));
					} catch (StringIndexOutOfBoundsException e) {
						newWords.add(word.substring(i * width));
					}
			}

		}
		for (String word : newWords) {
			if (sb.length() + word.length() == width)
				sb.append(word);
			else if (sb.length() + word.length() < width) {
				sb.append(word);
				sb.append(' ');
			} else {
				result.add(sb.toString());
				sb = new StringBuilder();
				sb.append(word);
				sb.append(' ');
			}
		}
		sb.setLength(sb.length() - 1);
		result.add(sb.toString());

		return result;
	}

}
