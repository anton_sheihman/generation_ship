package info.sheix.gs;

import com.badlogic.gdx.math.Vector2;
import info.sheix.gs.entities.SolarSystem;
import info.sheix.gs.entities.Starship;
import info.sheix.gs.entities.StarshipGalaxy;
import info.sheix.gs.util.Singleton;

/**
 * Created by Sheix on 12/21/2016.
 */
public class GameState {
    private GameMode gameMode = GameMode.LOGO;

    private Starship starship;
    private StarshipGalaxy starshipGalaxy;
    private Galaxy galaxy;
    private SolarSystem solarSystem;

    public GameState(Galaxy galaxy) {
        init(galaxy);

    }

    public void init(Galaxy galaxy) {
        this.galaxy = galaxy;
        starship = new Starship(true);
        starshipGalaxy = new StarshipGalaxy();
        starshipGalaxy.setLocation(new Vector2(3, 3));
        galaxy.updateVisibles(starshipGalaxy.getLocation(), starshipGalaxy.getVisibilityRadius());
    }

    public Starship getStarship() {
        return starship;
    }

    public void setStarship(Starship starship) {
        this.starship = starship;
    }

    public StarshipGalaxy getStarshipGalaxy() {
        return starshipGalaxy;
    }

    public void setStarshipGalaxy(StarshipGalaxy starshipGalaxy) {
        this.starshipGalaxy = starshipGalaxy;
    }

    public Galaxy getGalaxy() {
        return galaxy;
    }

    public void setGalaxy(Galaxy galaxy) {
        this.galaxy = galaxy;
    }

    public SolarSystem getDestination() {
        return galaxy.getDestination();
    }

    public SolarSystem getSolarSystem() {
        return solarSystem;
    }

    public void setSolarSystem(SolarSystem solarSystem) {
        this.solarSystem = solarSystem;
    }

    public GameMode getGameMode() {
        return gameMode;
    }

    public void setGameMode(GameMode gameMode) {
        if (Singleton.INSTANCE.debug) {
            System.out.println("Switching to state: " + gameMode.toString());
        }
        this.gameMode = gameMode;
    }
}
