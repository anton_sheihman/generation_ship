package info.sheix.gs;

import info.sheix.gs.util.Singleton;

public class Constants {

    public static final int galaxySizeX = 32;
    public static final int galaxySizeY = 32;

    public static final int turnsSinceLaunchToEnableDocking = 4;
    public static final float probeInteractionDistance = 27f;
    public static final int modeDefaultDelay = 10;

    public static final int gameX = 640;
    public static final int gameY = 480;

    public static int minimalDistanceFromSun = 10;
    public static int chanceOfGeneratingComet = Singleton.INSTANCE.debug ? 85 : 10;
    public static float twoPi = 6.28f;
    public static float starshipSpeed = 0.006f;

    public static final float stepThetaAcceleration = 0.03f;

    public static float probeStartDecelerationAngle = 0.1f;
    public static float probeMaxAcceleration = 0.10f;

}
