package info.sheix.gs;

import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.math.Vector2;
import info.sheix.gs.util.Singleton;

public class ParticleManager {

    public ParticleEffect createAndStartEffect(Vector2 probeVector, Effects effect) {
        ParticleEffect particleEffect;
        switch (effect) {
            case PROBE_EXPLODED:
                particleEffect = new ParticleEffect(Singleton.INSTANCE.getAssetManager().get("explosionEmitter.pe", ParticleEffect.class));
                break;
            case CREW_MEMBER_DIED:
                particleEffect = new ParticleEffect(Singleton.INSTANCE.getAssetManager().get("soulEmitter.pe", ParticleEffect.class));
                break;
            default:
                throw new RuntimeException("Effect is a must!");

        }
        particleEffect.setPosition(probeVector.x, probeVector.y);
        particleEffect.start();
        return particleEffect;
    }
}
