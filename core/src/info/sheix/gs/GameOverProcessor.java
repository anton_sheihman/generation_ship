/*
 * Copyright (c) 2017, Omnisol Ltd. All rights reserved.
 * OMNISOL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package info.sheix.gs;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import info.sheix.gs.interfaces.Renderer;
import info.sheix.gs.util.Singleton;

public class GameOverProcessor extends Renderer {


    @Override
    public void render(Batch batch, GameState gameState) {
        super.render(batch, gameState);
        BitmapFont font = Singleton.INSTANCE.getAssetManager().get("fonts/kongtext.fnt");
        String gameOverMessage;
        if (gameState.getSolarSystem().equals(gameState.getDestination()))
            gameOverMessage = "You have reached your destination!";
        else
            gameOverMessage = "You have lost!";

        gameOverMessage += "\nClick to continue";
        font.setColor(Color.LIGHT_GRAY);
        font.draw(batch, gameOverMessage, 20, 400);
    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        super.keyUp(keycode);
        gameState.setGameMode(GameMode.START);
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        gameState.setGameMode(GameMode.START);
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
