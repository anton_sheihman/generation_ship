/*
 * Copyright (c) 2017, Omnisol Ltd. All rights reserved.
 * OMNISOL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package info.sheix.gs;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.*;
import com.badlogic.gdx.math.Vector2;
import com.google.common.base.Strings;
import info.sheix.gs.entities.*;
import info.sheix.gs.interfaces.Renderer;
import info.sheix.gs.outcomes.CrashOutcome;
import info.sheix.gs.outcomes.OutcomeProcessor;
import info.sheix.gs.outcomes.TurnOutcome;
import info.sheix.gs.util.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static info.sheix.gs.Constants.gameX;
import static info.sheix.gs.Constants.gameY;
import static info.sheix.gs.GameMode.GAME_OVER;
import static info.sheix.gs.util.Sounds.Launch;

public class SolarSystemProcessor extends Renderer {
    private OrbitalObject selectedObject;
    private int centerY;
    private int centerX;
    private OutcomeProcessor outcomeProcessor = new OutcomeProcessor();
    private GlyphLayout layout = new GlyphLayout();
    private boolean canBeep = true;
    private List<ParticleEffect> particleEffects = new ArrayList<>();

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        super.keyUp(keycode);
        if (keycode == Input.Keys.F) {
            if (Singleton.INSTANCE.debug) {
                ResourceRecord data = new ResourceRecord(100, 0, 100);
                if (selectedObject instanceof ResourceHolder)
                    ((ResourceHolder) selectedObject).getResourceHolder().add(data);
            }
        }


        if (keycode == Input.Keys.E) {
            nextTurn();
        }

        if (keycode == Input.Keys.J) {
            gameState.setGameMode(GameMode.GALAXY);
        }

        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        if (super.touchUp(screenX, screenY, pointer, button))
            return false;

        if (button == Input.Buttons.RIGHT)
            if (canJump())
                gameState.setGameMode(GameMode.GALAXY);


        if (selectedObject != null) {
            if (canBeep) {
                SoundManager.play(Sounds.SelectStar);
                canBeep = false;
            }
            if (selectedObject instanceof Planet) {
                Probe probe = gameState.getStarship().launchProbe(selectedObject);
                if (probe != null) {
                    SoundManager.play(Launch);
                } else {
                    messages.add("Need to build more probes, click on starship to build one");
                    return false;
                }
            }
            if (selectedObject instanceof Starship) {
                messages.add(gameState.getStarship().buildProbe());
            }
        } else
            canBeep = true;

        nextTurn();

        return false;
    }

    private boolean canJump() {
        return gameState.getStarship().haveEnoughFuelToJump();
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }


    @Override
    public boolean scrolled(int amount) {
        return false;
    }

    @Override
    public void render(Batch batch, GameState gameState) {

        super.render(batch, gameState);
        SolarSystem solarSystem = gameState.getSolarSystem();

        if (solarSystem.equals(gameState.getDestination())) {
            gameState.setGameMode(GAME_OVER);
            return;
        }

        SpecialEvent event = solarSystem.getEvent();
        if (event != null) {
            String eventMessage = event.getMessage();
            SoundManager.play(Sounds.EventOnEnteringSolarSystem);
            messages.add(eventMessage);
            if (event.getResources() != null) {
                gameState.getStarship().getResourceHolder().add(event.getResources());
            }
            gameState.setStarship(event.getProcessingFunction().apply(gameState.getStarship()));

            solarSystem.endEvent();
        }

        centerX = gameX / 2;
        centerY = gameY / 2;

        List<OrbitalObject> orbitalObjects = new ArrayList<>();
        orbitalObjects.add(solarSystem.getSun());
        orbitalObjects.addAll(solarSystem.getPlanets());
        orbitalObjects.addAll(solarSystem.getComets());
        orbitalObjects.addAll(gameState.getStarship().getAwayProbes());
        orbitalObjects.add(gameState.getStarship());

        float minimal_distance = 10f;
        OrbitalObject nearestObject = null;
        for (OrbitalObject oo : orbitalObjects) {
            float originX = getXorigin(centerX, oo, oo.getSize());
            float originY = getYorigin(centerY, oo, oo.getSize());
            Vector2 v = getGameFromScreen(x, y);
            float distance = v.dst(originX, originY);
            //debugCursorCoordinates(x, y, v);
            if (distance < minimal_distance) {
                nearestObject = oo;
                minimal_distance = distance;
            }
            oo.draw(batch, originX, originY, Origin.SPRITE_CENTER, true);

        }
        selectedObject = nearestObject;

        if (selectedObject instanceof Sun)
            addHint("You shouldn't go here!");
        if (selectedObject instanceof Planet)
            addHint("Click to launch probe");
        if (selectedObject instanceof Starship)
            addHint("Click to build probe");
        if (selectedObject instanceof Comet)
            addHint("There is a comet! Avoid the collision!");
        if (selectedObject == null)
            addHint("Click for next turn");
        if (gameState.getStarship().haveEnoughFuelToJump())
            addHint("Right click to jump");

        if (nearestObject != null) {
            float originX = getXorigin(centerX, nearestObject, nearestObject.getSize());
            float originY = getYorigin(centerY, nearestObject, nearestObject.getSize());
            renderNearestObject(batch, nearestObject, originX, originY);
        }

        renderExplosions(batch);
        renderStats(batch);
        renderHints(batch);
    }

    private void renderExplosions(Batch batch) {
        particleEffects = particleEffects.stream()
                .filter(pe -> !pe.isComplete())
                .peek(ac -> ac.draw(batch, Gdx.graphics.getDeltaTime()))
                .collect(Collectors.toList());
    }

    private void makeTurn() {
        if (Singleton.INSTANCE.debug) {
            System.out.println("**** Start turn! ****");
        }
        List<OrbitalObject> orbitalObjects = new ArrayList<>(gameState.getSolarSystem().getPlanets());
        orbitalObjects.add(gameState.getStarship());
        orbitalObjects.addAll(gameState.getStarship().getProbes());
        orbitalObjects.addAll(gameState.getSolarSystem().getComets());

        for (OrbitalObject orbitalObject : orbitalObjects) {
            List<TurnOutcome> turnOutcomes = orbitalObject.makeTurn();
            turnOutcomes.forEach(turnOutcome ->
                    processOutcome(turnOutcome, getOrbitalObjectVector(orbitalObject))
            );
        }

        if (gameState.getStarship().isGameOver())
            gameState.setGameMode(GAME_OVER);
    }

    private void processOutcome(TurnOutcome turnOutcome, Vector2 orbitalObjectVector) {
        if (turnOutcome == null) {
            return;
        }
        if (!Strings.isNullOrEmpty(turnOutcome.getMessage()))
            messages.add(turnOutcome.getMessage());
        Effects effect = turnOutcome.getEffect();
        if (effect != null) {
            if (effect.equals(Effects.CREW_MEMBER_DIED)) {
                particleEffects.add(Singleton.INSTANCE.particleManager.createAndStartEffect(orbitalObjectVector, Effects.CREW_MEMBER_DIED));
            }
        }
    }

    private float degrees = 0;

    private void renderNearestObject(Batch batch, OrbitalObject oo, float originX, float originY) {
        rotate();
        Sprite crosshairSprite = new Sprite(Singleton.INSTANCE.getAssetManager().get("crosshair.png", Texture.class));

        crosshairSprite.setPosition(originX - 16, originY - 16);

        crosshairSprite.setRotation(degrees);
        crosshairSprite.draw(batch);
        renderSelectedOOInfo(batch, oo, originX, originY);
    }

    private void rotate() {
        degrees += 5;
        if (degrees > 360) degrees = 0;
    }

    private void renderSelectedOOInfo(Batch batch, OrbitalObject oo, float originX, float originY) {
        if (oo instanceof Planet)
            renderSelectedPlanetInfo(batch, (Planet) oo, originX, originY);
        if (oo instanceof Probe)
            renderSelectedProbe(batch, (Probe) oo, originX, originY);
        if (oo instanceof Sun)
            renderSelectedStarInfo(batch, oo, originX, originY);
    }

    private void renderSelectedStarInfo(Batch batch, OrbitalObject oo, float originX, float originY) {
        renderInfo(batch, oo.getInfo(), originX, originY);
    }

    private void renderSelectedProbe(Batch batch, Probe probe, float originX, float originY) {
        String probeInfo = " " + probe.getInfo();
        float x = getXorigin(centerX, probe.getProjectedRadialCoordinates(), 16);
        float y = getYorigin(centerY, probe.getProjectedRadialCoordinates(), 16);
        probe.drawProjected(batch, x, y);

        renderInfo(batch, probeInfo, originX, originY);

    }

    private void renderSelectedPlanetInfo(Batch batch, Planet planet, float originX, float originY) {
        renderInfo(batch, " " + planet.getInfo(), originX, originY);
    }

    private void renderInfo(Batch batch, String info, float originX, float originY) {
        BitmapFont font = Singleton.INSTANCE.getAssetManager().get("fonts/kongtext.fnt");
        layout.setText(font, info);
        if (layout.width + originX > Constants.gameX)
            originX = originX - layout.width;
        if (layout.height - originY > 0)
            originY = originY + layout.height;

        font.draw(batch, layout, originX, originY);
    }

    private float getYorigin(int centerY, OrbitalObject planet, int size) {
        return getYorigin(centerY, new Vector2(planet.getDistance(), planet.getTheta()), size);
    }

    private float getXorigin(int centerX, OrbitalObject planet, int size) {
        return getXorigin(centerX, new Vector2(planet.getDistance(), planet.getTheta()), size);
    }

    private float getXorigin(int centerX, Vector2 radial, int size) {
        return centerX - size / 2 + (float) ((radial.x) * Math.sin(radial.y));
    }

    private float getYorigin(int centerY, Vector2 radial, int size) {
        return centerY - size / 2 - (float) ((radial.x) * Math.cos(radial.y));
    }

    private Vector2 getGameFromScreen(int mouseX, int mouseY) {
        float newX = (float) mouseX / ((float) Singleton.INSTANCE.X / (float) gameX);
        float newY = (Singleton.INSTANCE.Y - (float) mouseY) / ((float) Singleton.INSTANCE.Y / (float) gameY);
        return new Vector2(newX, newY);
    }

    private void nextTurn() {
        makeTurn();
        for (Probe probe : gameState.getStarship().getProbes()) {

            if (!probe.isOnBoard()) {
                if (!probe.isFull()) {
                    for (Planet planet : gameState.getSolarSystem().getPlanets()) {
                        // take planet.size into account
                        if (probe.dst(planet) < Constants.probeInteractionDistance) {
                            Outcome outcome = outcomeProcessor.processOutcome(planet, probe);
                            if (Singleton.INSTANCE.debug)
                                System.out.println("Probe-planet Turn outcome: " + outcome.message);
                            String message = outcome.getMessage();
                            if (outcome instanceof CrashOutcome) {
                                killProbe(probe);
                            }
                            if (!isNullOrEmpty(message))
                                messages.add(message);
                        }
                    }
                }
                if (probe.dst(gameState.getStarship()) < Constants.probeInteractionDistance) {
                    if (probe.isFull() || probe.getTurnsSinceLaunch() > Constants.turnsSinceLaunchToEnableDocking) {
                        gameState.getStarship().dockProbe(probe);
                    }
                }
                if (probe.getDistance() < Constants.minimalDistanceFromSun) {
                    killProbe(probe);
                    messages.add(Messages.PROBE_GOT_NEAR_SUN);
                }
                for (Comet comet : gameState.getSolarSystem().getComets()) {
                    // take planet.size into account
                    if (probe.dst(comet) < Constants.probeInteractionDistance) {
                        Outcome outcome = outcomeProcessor.processOutcome(comet, probe);
                        if (Singleton.INSTANCE.debug)
                            System.out.println("Probe-comet Turn outcome: " + outcome.message);
                        String message = outcome.getMessage();
                        if (outcome instanceof CrashOutcome) {
                            killProbe(probe);

                        }
                        if (!isNullOrEmpty(message))
                            messages.add(message);
                    }
                }

            }
        }
    }

    private void killProbe(Probe probe) {
        if (Singleton.INSTANCE.debug) {
            System.out.println("Crash probe " + probe);
        }
        probe.kill();
        SoundManager.play(Sounds.CrashSound);
        Vector2 probeVector = getOrbitalObjectVector(probe);
        particleEffects.add(Singleton.INSTANCE.particleManager.createAndStartEffect(probeVector, Effects.PROBE_EXPLODED));
    }


    private boolean isNullOrEmpty(String message) {
        return message == null || message.isEmpty();
    }

    private Vector2 getOrbitalObjectVector(OrbitalObject orbitalObject) {
        int size = orbitalObject.getSize();
        float originX = getXorigin(centerX, orbitalObject, size);
        float originY = getYorigin(centerY, orbitalObject, size);
        return new Vector2(originX, originY);
    }

}
