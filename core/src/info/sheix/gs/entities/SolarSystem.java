package info.sheix.gs.entities;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import info.sheix.gs.Origin;
import info.sheix.gs.util.SpecialEvent;
import info.sheix.gs.util.Singleton;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class SolarSystem extends AbstractDrawable {
    public static SolarSystem noSolarSystem = new SolarSystem(null, new ArrayList<Planet>(), null, null);

    private static int idCounter = 1;

    private int id;

    private Sun sun;
    private List<Planet> planets;
    private float alpha;
    private boolean increaseAlpha = true;

    private int isBlinking;

    private Random r = Singleton.INSTANCE.getRandom();
    private boolean visible;
    private SpecialEvent event;
    private List<Comet> comets;

    public SolarSystem(Sun sun, List<Planet> planets, Sprite sprite, SpecialEvent event) {
        this.event = event;

        id = idCounter;
        idCounter++;

        this.planets = planets;
        this.sun = sun;
        this.sprite = sprite;
        this.setVisible(false);
        if (this.sprite != null) {
            float amount = (float) planets.size() / 8f + 0.1f;
            this.sprite.scale(amount);
        }
    }

    public List<Planet> getPlanets() {
        return planets;
    }

    public void setPlanets(List<Planet> planets) {
        this.planets = planets;
    }

    public Sun getSun() {
        return sun;
    }

    public void setSun(Sun sun) {
        this.sun = sun;
    }


    @Override
    public String getInfo() {
        return "Star system " + id;
    }

    public float getAlpha() {
        if (r == null)
            return alpha;
        if (r.nextInt(10000) == 0)
            isBlinking = 20;
        if (isBlinking == 0)
            return (visible ? 1f : 0.5f);
        else {
            if (alpha > 1)
                increaseAlpha = false;
            if (alpha < 0)
                increaseAlpha = true;
            if (increaseAlpha)
                alpha = alpha + 0.1f;
            else
                alpha = alpha - 0.1f;
            isBlinking--;
        }
        return alpha;
    }

    public Color getStarColor() {
        if (sun == null)
            return Color.BLACK;
        return sun.getSunColor();
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    @Override
    public String toString() {
        return "Solar system (" + id + ")";

    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + id;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        SolarSystem other = (SolarSystem) obj;
        if (id != other.id)
            return false;
        return true;
    }

    public SpecialEvent getEvent() {
        return event;
    }

    public void endEvent() {
        event = null;
    }

    public void draw(Batch batch, float x, float y, Origin spriteCenter, boolean highlighted) {
        getSprite().setColor(getStarColor());
        getSprite().setAlpha(getAlpha());
        if (highlighted)
            getSprite().setRotation(Singleton.INSTANCE.getRandom().nextInt(90));
        super.draw(batch, x, y, spriteCenter, false);
    }

    public void setComets(List<Comet> comets) {
        this.comets = comets;
    }

    public List<Comet> getComets() {
        return comets;
    }
}
