package info.sheix.gs.entities;

import info.sheix.gs.ResourceRecord;

/**
 * Created by Sheix on 11/7/2016.
 */
public interface ResourceHolder {
    ResourceRecord getResourceHolder();

}
