package info.sheix.gs.entities;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import info.sheix.gs.Origin;
import info.sheix.gs.Outcome;
import info.sheix.gs.ResourceRecord;
import info.sheix.gs.outcomes.*;
import info.sheix.gs.util.Singleton;

import java.util.ArrayList;
import java.util.List;

import static info.sheix.gs.util.TimeUtils.shouldBlink;

public class Planet extends OrbitalObject {

    private final Sprite SosSprite;
    private Sprite VSprite;
    private boolean rescueMissionAvailable;

    public Planet(Sprite sprite) {
        this.setSprite(sprite);
        this.setVSprite(new Sprite(Singleton.INSTANCE.getAssetManager().get("mark_v.png", Texture.class)));
        this.SosSprite = new Sprite(Singleton.INSTANCE.getAssetManager().get("sos.png", Texture.class));

        data = new ResourceRecord();
    }

    @Override
    public String toString() {
        return getName() + " [distance=" + distance + ", angularSpeed=" + angularSpeed + ", size=" + size + "] " + (rock ? "r" : " ")
                + (metal ? "m" : " ") + (air ? "a" : " ") + (life ? "l" : " ") + (populated ? "p" : " ")
                + (smart ? "s" : " ");
    }

    private int size;

    private boolean rock;
    private boolean metal;
    private boolean air;
    private boolean life;
    private boolean populated;
    private boolean smart;

    private boolean visited;

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    @Override
    public String getInfo() {
        return getName() + "\n" + (rock ? "Rocky" : "Gas planet") + "\n"
                + (metal ? "Metals discovered\n" : "") + (air ? "Atmosphere detected\n" : "") + (life ? "Signs of life\n" : "") + (populated ? "Populated by animals\n" : "")
                + (smart ? "Intelligent life\n" : "");
    }

    public boolean isRock() {
        return rock;
    }

    public void setRock(boolean rock) {
        this.rock = rock;
    }

    public boolean isMetal() {
        return metal;
    }

    public void setMetal(boolean metal) {
        this.metal = metal;
    }

    public boolean isAir() {
        return air;
    }

    public void setAir(boolean air) {
        this.air = air;
    }

    public boolean isLife() {
        return life;
    }

    public void setLife(boolean life) {
        this.life = life;
    }

    public boolean isPopulated() {
        return populated;
    }

    public void setPopulated(boolean populated) {
        this.populated = populated;
    }

    public boolean isSmart() {
        return smart;
    }

    public void setSmart(boolean smart) {
        this.smart = smart;
    }

    public void setSprite(Sprite sprite) {
        this.sprite = sprite;
    }

    @Override
    public void processOutcome(Outcome o) {
        if (!(o instanceof CrashOutcome)) {
            visited = true;
        }
        if (o instanceof CrashOutcome) {
            if (Singleton.INSTANCE.getRandom().nextInt(100) < 25) {
                setRescueMissionAvailable(o.data);
            }

        }

        if (o instanceof RescueOutcome) {
            setRescueMissionNotAvailable();
            data = new ResourceRecord();
        }
    }

    private void setRescueMissionNotAvailable() {
        rescueMissionAvailable = false;
    }

    private void setRescueMissionAvailable(ResourceRecord data) {
        rescueMissionAvailable = true;
        this.data.add(data);
    }

    public boolean isVisited() {
        return visited;
    }

    public void draw(Batch batch, float originX, float originY, Origin origin, boolean addOffset) {
        if (isVisited() && !isRescueMissionAvailable())
            batch.draw(VSprite, originX, originY);
        if (isRescueMissionAvailable())
            if (shouldBlink()) {
                batch.draw(SosSprite, originX, originY);
            }
        super.draw(batch, originX, originY, origin, addOffset);
    }

    public void setVSprite(Sprite VSprite) {
        this.VSprite = VSprite;
    }

    public List<Outcome> getPossibleOutcomes(Probe probe) {
        List<Outcome> possibleOutcomes = new ArrayList<>();

        possibleOutcomes.add(new CrashOutcome(probe.data));


        possibleOutcomes.add(new NothingHappenedOutcome());
        if (isRock()) {
            possibleOutcomes.add(new LandedSuccessfullyOutcome());
            if (Singleton.INSTANCE.getRandom().nextInt(2) == 0) {
                possibleOutcomes.add(new CrashOutcome(probe.data));
            }
        }
        if (isMetal())
            possibleOutcomes.add(new GatheredMetalOutcome());

        if (isLife())
            possibleOutcomes.add(new GatheredFoodOutcome());

        if (isSmart())
            possibleOutcomes.add(new AddedCrewOutcome());

        if (rescueMissionAvailable)
            possibleOutcomes.add(new RescueOutcome(data));

        return possibleOutcomes;
    }

    public boolean isRescueMissionAvailable() {
        return rescueMissionAvailable;
    }

    @Override
    public List<TurnOutcome> makeTurn() {
        List<TurnOutcome> turnOutcomes = super.makeTurn();

        if (data.crew != 0)
            if (isAir()) {
                if (Singleton.INSTANCE.getRandom().nextBoolean()) {
                    data.food += 1;
                }
            }

        if (isMetal()) {
            if (data.crew > 1) {
                if (Singleton.INSTANCE.getRandom().nextBoolean())
                    data.metal += Singleton.INSTANCE.getRandom().nextInt(3) + 1;
            }
        }
        if (isLife()) {
            if (Singleton.INSTANCE.getRandom().nextBoolean())
                data.food += Singleton.INSTANCE.getRandom().nextInt(3) + 1;
            if (Singleton.INSTANCE.getRandom().nextBoolean())
                data.crew--;
        }
        if (isSmart()) {
            if (Singleton.INSTANCE.getRandom().nextBoolean())
                data.food += Singleton.INSTANCE.getRandom().nextInt(5) + 1;
            if (Singleton.INSTANCE.getRandom().nextBoolean())
                data.crew += Singleton.INSTANCE.getRandom().nextInt(5) - 2;
        }

        return turnOutcomes;
    }
}
