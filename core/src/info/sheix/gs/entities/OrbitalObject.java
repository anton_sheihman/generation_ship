package info.sheix.gs.entities;

import info.sheix.gs.Constants;
import info.sheix.gs.Outcome;
import info.sheix.gs.ResourceRecord;
import info.sheix.gs.outcomes.TurnOutcome;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static java.lang.Math.*;

public abstract class OrbitalObject extends AbstractDrawable implements Turnable {

    protected ResourceRecord data = new ResourceRecord();

    public static OrbitalObject EmptyTarget = new OrbitalObject() {
        @Override
        public void processOutcome(Outcome o) {

        }

        @Override
        public String getInfo() {
            return "";
        }
    };
    protected int distance;
    protected float angularSpeed;
    private float theta;

    private String name;

    public double dst(OrbitalObject oo) {
        return dst(oo.distance, oo.theta);
    }

    public double dst(int otherDistance, float otherTheta) {
        return sqrt(distance * distance + otherDistance * otherDistance - 2f * distance * otherDistance * cos(theta - otherTheta));
    }

    private static int distance(int alpha, int beta) {
        int phi = abs(beta - alpha) % 360;       // This is either the distance or 360 - distance
        int distance = phi > 180 ? 360 - phi : phi;

        int sign = (alpha - beta >= 0 && alpha - beta <= 180) || (alpha - beta <= -180 && alpha - beta >= -360) ? 1 : -1;
        distance *= sign;
        return distance;
    }


    public double angularDistance(OrbitalObject oo) {
        int distance = distance(((int) toDegrees(this.getTheta())), (int) toDegrees(oo.getTheta()));
        return toRadians(distance);
    }

    public OrbitalObject(int distance, float angularSpeed, float theta) {
        this.distance = distance;
        this.angularSpeed = angularSpeed;
        this.theta = theta;
    }

    public OrbitalObject() {
        super();
    }

    public float getTheta() {
        return theta;
    }

    public void setTheta(float theta) {
        this.theta = theta;
    }

    public int getDistance() {
        return distance;
    }


    public void setDistance(int distance) {
        this.distance = distance;
    }

    public float getAngularSpeed() {
        return angularSpeed;
    }

    public void setAngularSpeed(float angularSpeed) {
        this.angularSpeed = angularSpeed;
    }

    @Override
    public List<TurnOutcome> makeTurn() {
        updateLocation();
        ArrayList<TurnOutcome> turnOutcomes = new ArrayList<>();
        turnOutcomes.add(data.makeTurn());
        return turnOutcomes;
    }

    protected void updateLocation() {
        updateAngularPosition();
    }

    public void updateAngularPosition() {
        theta += angularSpeed;

        if (theta >= Constants.twoPi)
            theta = theta - Constants.twoPi;
        if (theta <= 0)
            theta = theta + Constants.twoPi;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public abstract void processOutcome(Outcome o);

    @Override
    public String toString() {
        return "name: " + name + ", distance=" + distance + ", angularSpeed=" + angularSpeed + ", theta=" + theta;
    }
}