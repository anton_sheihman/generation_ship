package info.sheix.gs.entities;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import info.sheix.gs.util.Singleton;

public class StarshipGalaxy extends AbstractDrawable {
    private Vector2 location;
    private int visibilityRadius;

    public StarshipGalaxy() {
        setVisibilityRadius(12);
        sprite = new Sprite(Singleton.INSTANCE.getAssetManager().get("ship.png", Texture.class));
    }

    @Override
    public String getInfo() {
        return null;
    }

    public void setLocation(Vector2 location) {
        this.location = location;

    }

    public Vector2 getLocation() {
        return location;
    }

    public int getVisibilityRadius() {
        return visibilityRadius;
    }

    public void setVisibilityRadius(int visibilityRadius) {
        this.visibilityRadius = visibilityRadius;
    }

}
