package info.sheix.gs.entities.generators;

import info.sheix.gs.entities.Comet;

import java.util.Random;

public class CometGenerator {

    public Comet getNewComet(Random r, int distance) {
        float angularSpeed = 0.006f;
        float angularLocation = r.nextInt(628) / 100f;
        float angularAcceleration = 0.05f;
        return new Comet(angularAcceleration, 0.0324f, 5, angularSpeed, angularLocation, distance);
    }
}