package info.sheix.gs.entities.generators;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import info.sheix.gs.Constants;
import info.sheix.gs.ResourceRecord;
import info.sheix.gs.entities.Comet;
import info.sheix.gs.entities.Planet;
import info.sheix.gs.entities.SolarSystem;
import info.sheix.gs.entities.Sun;
import info.sheix.gs.util.Singleton;
import info.sheix.gs.util.SpecialEvent;
import info.sheix.gs.util.name_generator.NamingAlgorithm;
import info.sheix.gs.util.name_generator.StringFilter;
import info.sheix.gs.util.name_generator.StringGenerator;
import info.sheix.gs.util.name_generator.StringProcessor;
import info.sheix.gs.util.name_generator.implementation.CapitalizeProcessor;
import info.sheix.gs.util.name_generator.implementation.CountProcessor;
import info.sheix.gs.util.name_generator.implementation.LongerThanXFilter;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static java.lang.Math.max;

public class SolarSystemGenerator {

    private static int maxPlanets;
    private Random r;
    PlanetGenerator planetGenerator;
    private CometGenerator cometGenerator;

    public SolarSystemGenerator(Random r) {
        this.r = r;
        planetGenerator = new PlanetGenerator();
        cometGenerator = new CometGenerator();
    }

    public SolarSystem generateSolarSystem() {

        Sun sun = new Sun(new Sprite(Singleton.INSTANCE.getAssetManager().get("planet6.png", Texture.class)));

        sun.setSize(r.nextInt(3) + 1);
        sun.setColor(getRandomSunColor(r));


        List<Planet> planets = new ArrayList<>();
        List<Comet> comets = new ArrayList<>();
        int maxDistance = 290;
        maxPlanets = r.nextInt(11);
        int distance = 30 + sun.getSize() * 16;
        while (distance < maxDistance) {
            if (createPlanetHere(r, planets.size())) {
                if (r.nextInt(100) < Constants.chanceOfGeneratingComet)
                    comets.add(cometGenerator.getNewComet(r, distance));

                planets.add(planetGenerator.getNewPlanet(r, distance, new Sprite(Singleton.INSTANCE.getAssetManager().get(getRandomPlanetSpriteName(), Texture.class))));
                distance += planets.get(planets.size() - 1).getSize() + 1;
            }

            distance += r.nextInt(50) + r.nextInt(10) + 10;
        }

        List<String> planetNames = Singleton.INSTANCE.getNameGenerator().generateNames(createAlgorithm(), planets.size());
        for (Planet planet : planets) {
            planet.setName(planetNames.remove(0));
        }
        SpecialEvent event = createSpecialEvent();

        SolarSystem solarSystem = new SolarSystem(sun, planets, new Sprite(Singleton.INSTANCE.getAssetManager().get("star.png", Texture.class)), event);
        solarSystem.setComets(comets);
        return solarSystem;
    }

    private SpecialEvent createSpecialEvent() {
        switch (r.nextInt(50)) {
            case 0: // duplicate with
                return new SpecialEvent("You've found a derelict spaceship right where you appeared. Some scrap metal gathered.", new ResourceRecord(0, 0, 10), s -> s);
            case 1: // duplicate it with virus
                return new SpecialEvent("You've found a derelict spaceship right where you appeared. There is alive person in cryochamber!", new ResourceRecord(0, 1, 0), s -> s);
            case 2:
                return new SpecialEvent("There is a probe, almost untouched. It's inhabitants gone.", new ResourceRecord(0, 0, 10), s -> {
                    s.buildProbe();
                    return s;
                });
            case 3:
                return new SpecialEvent("There is a sudden surge of power in your ship's zero drive!", null, s -> {
                    s.setZeroDriveFuel(s.getZeroDriveFuelToJump());
                    return s;
                });
            case 4:
                return new SpecialEvent("One of crew members feels enlightened and proposes a better way to utilize zero-drive fuel!", new ResourceRecord(), s -> {
                    s.setZeroDriveFuelToJump(s.getZeroDriveFuelToJump() - 1);
                    return s;
                });
        }
        return SpecialEvent.NothingHappened;
    }

    private NamingAlgorithm createAlgorithm() {
        List<StringFilter> filters = new ArrayList<>();
        List<StringProcessor> processors = new ArrayList<>();
        StringGenerator stringsGenerator;


        NamingAlgorithm namingAlgorithm;
        int roll = r.nextInt(2);
        switch (roll) {
            case 0:
                stringsGenerator = Singleton.INSTANCE.getProbesGenerator()::generateSameWithMinLength5;
                filters.add(new LongerThanXFilter(2));
                processors.add(new CapitalizeProcessor());
                processors.add(new CountProcessor());
                break;
            case 1:
                stringsGenerator = Singleton.INSTANCE.getProbesGenerator()::generateDifferent;
                filters.add(new LongerThanXFilter(2));
                processors.add(new CapitalizeProcessor());
                break;
            default:
                return null;
        }

        namingAlgorithm = new NamingAlgorithm(stringsGenerator, filters, processors);

        return namingAlgorithm;
    }

    private String getRandomPlanetSpriteName() {
        int no = r.nextInt(12) + 1;
        return "planet" + no + ".png";

    }

    private static boolean createPlanetHere(Random r, int size) {
        return r.nextInt(max(11, maxPlanets)) > size;
    }

    private static Color getRandomSunColor(Random r) {
        int color = r.nextInt(5) + 1;
        switch (color) {
            case 1:
                return Color.YELLOW;

            case 2:
                return Color.RED;

            case 3:
                return Color.WHITE;

            case 4:
                return Color.BLUE;

            case 5:
                return Color.SCARLET;

            default:
                break;
        }
        return null;
    }

}
