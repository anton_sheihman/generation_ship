package info.sheix.gs.entities.generators;

import com.badlogic.gdx.graphics.g2d.Sprite;
import info.sheix.gs.entities.Planet;

import java.util.Random;


public class PlanetGenerator {


    public Planet getNewPlanet(Random r, int distance, Sprite sprite) {
        Planet planet = new Planet(sprite);
        planet.setDistance(distance);
        planet.setAngularSpeed(10 * r.nextFloat() / (float) distance);
        planet.setTheta(r.nextInt(628) / 100f);
        planet.setSize((r.nextInt(10) + r.nextInt(10) + 2 + distance / 20) / 2);
        planet.setRock(r.nextInt(600) > distance); // rock or gas
        planet.setMetal(planet.isRock() && r.nextInt(3) == 0); // geosphere
        planet.setAir(r.nextInt(10) == 0); // atmosphere
        planet.setLife(planet.isAir() ? r.nextInt(3) == 0 : r.nextInt(10) == 0); // biosphere
        planet.setPopulated(planet.isLife() && r.nextInt(4) == 0); //
        planet.setSmart(planet.isPopulated() ? r.nextInt(5) == 0 : r.nextInt(10) == 0);
        sprite.setSize(planet.getSize(), planet.getSize());
        return planet;
    }

}
