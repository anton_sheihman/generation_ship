package info.sheix.gs.entities.generators;

import info.sheix.gs.Constants;
import info.sheix.gs.Galaxy;
import info.sheix.gs.entities.Destination;
import info.sheix.gs.entities.Planet;
import info.sheix.gs.entities.SolarSystem;
import info.sheix.gs.util.Singleton;

import java.util.ArrayList;
import java.util.Random;

public class GalaxyGenerator {


	public GalaxyGenerator() {
	}

	public Galaxy generateGalaxy() {
		Random r = Singleton.INSTANCE.getRandom();
		SolarSystemGenerator solarSystemGenerator = new SolarSystemGenerator(r);
		Galaxy galaxy = new Galaxy();
		for (int i = 0; i < Constants.galaxySizeX; i++) {
			for (int j = 0; j < Constants.galaxySizeY; j++) {
					galaxy.setSolarSystem(i, j, SolarSystem.noSolarSystem);
			}
		}

		for (int i = 0; i < Constants.galaxySizeX / 8; i++) {
			for (int j = 0; j < Constants.galaxySizeY / 8; j++) {
				int x = r.nextInt(8);
				int y = r.nextInt(8);
				galaxy.setSolarSystem(i * 8 + x, j * 8 + y, solarSystemGenerator.generateSolarSystem());
			}
		}
		Destination destination = new Destination(null, new ArrayList<Planet>());

		galaxy.setSolarSystem(Constants.galaxySizeX - 3, Constants.galaxySizeY - 3, destination);

		return galaxy;
	}


}
