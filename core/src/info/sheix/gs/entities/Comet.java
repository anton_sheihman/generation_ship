/*
 * Copyright (c) 2017, Omnisol Ltd. All rights reserved.
 * OMNISOL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */

package info.sheix.gs.entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.ParticleEmitter;
import info.sheix.gs.Origin;
import info.sheix.gs.Outcome;
import info.sheix.gs.util.Singleton;

public class Comet extends EnginePoweredOrbitalObject {
    private float angleSpeedDelta;
    private int gravitySpeedDelta;

    private float angleSpeedDirection;
    private int gravitySpeedDirection;
    private ParticleEffect particleEffect;

    public Comet(float angularAcceleration, float maxAngleSpeed, int maxGravitySpeed, float angularSpeed, float angularLocation, int distance) {
        this.maxAngleSpeed = maxAngleSpeed;
        this.maxGravitySpeed = maxGravitySpeed;
        this.setThetaAcceleration(angularAcceleration);
        this.setAngularSpeed(angularSpeed);
        this.setTheta(angularLocation);
        this.setDistance(distance);
    }

    float maxAngleSpeed;
    int maxGravitySpeed;
    int gravitySpeed = 0;
    int gravityDirection = -1;

    @Override
    public void updateLocation() {
        updateGravitySpeed();

        super.updateLocation();
    }

    private void updateGravitySpeed() {
        gravitySpeed += gravityDirection;
        if (Math.abs(gravitySpeed) > maxGravitySpeed)
            gravitySpeed = -1 * gravitySpeed;
    }

    @Override
    public void processOutcome(Outcome o) {

    }

    @Override
    public void draw(Batch batch, float originX, float originY, Origin origin, boolean addOffset) {
        if (particleEffect == null) {
            particleEffect = new ParticleEffect(Singleton.INSTANCE.getAssetManager().get("cometEmitter.pe", ParticleEffect.class));
            particleEffect.start();
            particleEffect.scaleEffect(0.3f);
        }
        particleEffect.setPosition(originX, originY);
        particleEffect.draw(batch, Gdx.graphics.getDeltaTime());
        for (ParticleEmitter item : particleEffect.getEmitters()) {
            float targetAngle = getTheta() * 57.2958f - 90;

            rotateEmitterToAngle(item, targetAngle);
        }

    }

    private void rotateEmitterToAngle(ParticleEmitter item, float targetAngle) {
        ParticleEmitter.ScaledNumericValue angle = item.getAngle();
        float angleHighMin = angle.getHighMin();
        float angleHighMax = angle.getHighMax();
        float spanHigh = angleHighMax - angleHighMin;

        angle.setHigh(targetAngle - spanHigh / 2.0f, targetAngle + spanHigh / 2.0f);

        float angleLowMin = angle.getLowMin();
        float angleLowMax = angle.getLowMax();
        float spanLow = angleLowMax - angleLowMin;
        angle.setLow(targetAngle - spanLow / 2.0f, targetAngle + spanLow / 2.0f);
    }

    @Override
    public String getInfo() {
        return toString();
    }

    @Override
    public String toString() {
        return "Comet{" +
                "angleSpeedDelta=" + angleSpeedDelta +
                ", gravitySpeedDelta=" + gravitySpeedDelta +
                ", angleSpeedDirection=" + angleSpeedDirection +
                ", gravitySpeedDirection=" + gravitySpeedDirection +
                ", maxAngleSpeed=" + maxAngleSpeed +
                ", maxGravitySpeed=" + maxGravitySpeed +
                ", distance=" + distance +
                ", angularSpeed=" + angularSpeed +
                '}';
    }
}
