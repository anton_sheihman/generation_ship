package info.sheix.gs.entities;

import com.badlogic.gdx.graphics.g2d.Sprite;

public interface SpriteHolder {
	Sprite getSprite();
	
}
