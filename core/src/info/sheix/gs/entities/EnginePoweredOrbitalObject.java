package info.sheix.gs.entities;

import com.badlogic.gdx.math.Vector2;

public abstract class EnginePoweredOrbitalObject extends OrbitalObject {
    protected float thetaAcceleration;
    protected int gravitySpeed;
    protected boolean scheduledForRemove = false;
    protected int gravityAcceleration;

    protected EnginePoweredOrbitalObject() {
    }

    @Override
    public String toString() {
        return "EnginePoweredOrbitalObject{" +
                "thetaAcceleration=" + thetaAcceleration +
                ", gravitySpeed=" + gravitySpeed +
                ", distance=" + distance +
                ", angularSpeed=" + angularSpeed +
                '}';
    }

    public int getGravitySpeed() {
        return gravitySpeed;
    }

    public void setGravitySpeed(int gravitySpeed) {
        this.gravitySpeed = gravitySpeed;
    }

    public float getThetaAcceleration() {
        return thetaAcceleration;
    }

    public void setThetaAcceleration(float thetaAcceleration) {
        this.thetaAcceleration = thetaAcceleration;
    }

    protected void updateLocation() {
        this.setAngularSpeed(this.getAngularSpeed() + thetaAcceleration);
        this.setGravitySpeed(this.getGravitySpeed() + gravityAcceleration);
        updateDistance();
        super.updateLocation();
    }

    private void updateDistance() {
        distance = distance + gravitySpeed;
    }

    public Vector2 getProjectedRadialCoordinates() {
        return new Vector2(getDistance() + gravitySpeed, getTheta() + thetaAcceleration);
    }

    public void setGravityAcceleration(int gravityAcceleration) {
        this.gravityAcceleration = gravityAcceleration;
    }
}
