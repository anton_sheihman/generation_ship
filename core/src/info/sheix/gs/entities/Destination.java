package info.sheix.gs.entities;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import info.sheix.gs.Constants;
import info.sheix.gs.util.Singleton;

import java.util.List;

public class Destination extends SolarSystem {

	public Destination(Sun sun, List<Planet> planets) {
		super(sun, planets, new Sprite(Singleton.INSTANCE.getAssetManager().get("earth.png", Texture.class)), null);

	}

	@Override
	public com.badlogic.gdx.graphics.Color getStarColor() {
		return Color.WHITE;
	}

	public Vector2 getLocation() {
		return new Vector2(Constants.galaxySizeX - 3, Constants.galaxySizeY - 3);
	}

}
