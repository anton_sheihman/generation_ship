package info.sheix.gs.entities;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import info.sheix.gs.Constants;
import info.sheix.gs.Effects;
import info.sheix.gs.Outcome;
import info.sheix.gs.ResourceRecord;
import info.sheix.gs.outcomes.CrashOutcome;
import info.sheix.gs.outcomes.TurnOutcome;
import info.sheix.gs.util.Messages;
import info.sheix.gs.util.Singleton;
import info.sheix.gs.util.SoundManager;
import info.sheix.gs.util.Sounds;
import info.sheix.gs.util.name_generator.NamingAlgorithm;
import info.sheix.gs.util.name_generator.StringProcessor;
import info.sheix.gs.util.name_generator.implementation.CapitalizeProcessor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static info.sheix.gs.Constants.*;

public class Probe extends EnginePoweredOrbitalObject implements ResourceHolder {

    public static float probeMaxSpeed = 0.3f;
    public static float probeStartingSpeed = 0.01f;

    private static final int maxGravitySpeed = 30;
    private static final int approachGravitySpeed = 5;
    private int stepGravityAcceleration = 1;
    private static final int maxGravityAcceleration = 5;
    private final Starship homeBase;

    private Sprite projectedSprite;

    private int turnsSinceLaunch;
    private boolean onBoard;
    private boolean isFull;
    private OrbitalObject target;
    private boolean building;
    private int progress = 0;
    private double passedDistanceFromTarget;
    private int absoluteLaunchDistance;
    private int oldDirectionToGo;
    private int oldThetaDirection;

    public Probe(Starship starship, Boolean real) {
        homeBase = starship;
        data = new ResourceRecord();
        onBoard = true;
        building = true;
        isFull = false;
        if (real) {
            this.sprite = new Sprite(Singleton.INSTANCE.getAssetManager().get("probe.png", Texture.class));
            projectedSprite = new Sprite(sprite);
            setName(Singleton.INSTANCE.getNameGenerator().generateNames(getNamingAlgorithm(), 1).get(0));
        } else {
            setName("DummyProbe");
        }

        if (Singleton.INSTANCE.debug) {
            System.out.println(this);
        }

    }

    private NamingAlgorithm getNamingAlgorithm() {
        return new NamingAlgorithm(Singleton.INSTANCE.getProbesGenerator()::generateDifferent, Collections.emptyList(), getProcessors());
    }

    private List<StringProcessor> getProcessors() {
        return Collections.singletonList(new CapitalizeProcessor());
    }


    public boolean isOnBoard() {
        return onBoard;
    }

    public void setOnBoard(boolean onBoard) {
        if (!onBoard)
            setTurnsSinceLaunch(0);

        this.onBoard = onBoard;
    }

    @Override
    public List<TurnOutcome> makeTurn() {
        setTurnsSinceLaunch(getTurnsSinceLaunch() + 1);

        List<TurnOutcome> turnOutcomes = new ArrayList<>();
        if (!isOnBoard()) {
            turnOutcomes = new ArrayList<>(super.makeTurn());
            if ((getResourceHolder().food == 0) && (getResourceHolder().crew > 0)) {
                turnOutcomes.add(new TurnOutcome(Messages.CREW_MEMBER_DIED, Effects.CREW_MEMBER_DIED));
                getResourceHolder().crew -= 1;
            }

        }

        if (isBuilding()) {
            progress += 1;
        }
        if (progress == 3 && isBuilding()) {
            setBuilding(false);
            onBoard = true;
            SoundManager.play(Sounds.BuildProbe);
            return Collections.singletonList(new TurnOutcome(Messages.BUILD_SUCCEEDED));
        }

        if (data.crew == 0)
            setTarget(EmptyTarget);

        assert target != null;
        if (target.equals(EmptyTarget)) return turnOutcomes;
        if (Singleton.INSTANCE.debug) {
            System.out.println(this);
            System.out.println(" -> " + target);
        }
        //re-adjust target, for better aiming
        if (turnsSinceLaunch % 5 == 0) {
            this.setTarget(target);
        }
        adjustGravityAcceleration();
        adjustThetaAcceleration();
        return turnOutcomes;
    }

    private void adjustThetaAcceleration() {
        double thetaVector = angularDistance(target);

        int thetaDirection = -(int) Math.signum(thetaVector);
        if (turnsSinceLaunch == 1) {
            thetaAcceleration = thetaDirection * stepThetaAcceleration;
            oldThetaDirection = thetaDirection;
            return;
        }
        if (thetaDirection != oldThetaDirection) {
            oldThetaDirection = thetaDirection;
            thetaAcceleration *= -1;
            return;
        }

        if (acceleratingTheta) {
            if (Math.abs(angularSpeed) > probeMaxSpeed) {
                acceleratingTheta = false;
                setThetaAcceleration(0);
                return;
            }
            if (Math.abs(thetaVector) < Constants.probeStartDecelerationAngle) {
                acceleratingTheta = false;
                thetaAcceleration *= -1;
                return;
            }
            accelerateTheta();
        } else {
            decelerateTheta();
        }
    }

    private void accelerateTheta() {
        if (thetaAcceleration > 0)
            setThetaAcceleration(thetaAcceleration + stepThetaAcceleration);
        if (thetaAcceleration < 0)
            setThetaAcceleration(thetaAcceleration - stepThetaAcceleration);
    }

    private void decelerateTheta() {
        if (thetaAcceleration < 0)
            setThetaAcceleration(thetaAcceleration + stepThetaAcceleration);
        if (thetaAcceleration > 0)
            setThetaAcceleration(thetaAcceleration - stepThetaAcceleration);
    }

    private boolean acceleratingGravityDistance = true;
    private boolean acceleratingTheta = true;

    private void adjustGravityAcceleration() {
        int gravityDistance = target.getDistance() - getDistance();
        int directionToGo = (int) Math.signum(gravityDistance);
        int probeDirection = (int) Math.signum(gravitySpeed);
        int absoluteGravityDistance = Math.abs(gravityDistance);

        if (turnsSinceLaunch == 1) {
            oldDirectionToGo = directionToGo;
        }

        stepGravityAcceleration = ((int) Math.log(absoluteLaunchDistance));
        if (turnsSinceLaunch < 2) {
            gravityAcceleration = directionToGo * stepGravityAcceleration;
            return;
        }

        if (directionToGo != oldDirectionToGo) {
            oldDirectionToGo = directionToGo;
            gravityAcceleration *= -1;
        }

        if (acceleratingGravityDistance) {
            accelerateGravity(directionToGo);
            if (absoluteLaunchDistance * 2 < absoluteGravityDistance) {
                acceleratingGravityDistance = false;
                gravityAcceleration *= -1;
            }

        } else {
            decelerateGravity(probeDirection);
        }
    }


    @Override
    public void setThetaAcceleration(float thetaAcceleration) {
        if ((thetaAcceleration > Constants.probeMaxAcceleration) || (thetaAcceleration * -1 > Constants.probeMaxAcceleration))
            return;
        super.setThetaAcceleration(thetaAcceleration);
    }

    @Override
    protected void updateLocation() {
        int oldDistance = getDistance();
        float oldTheta = getTheta();

        super.updateLocation();

        passedDistanceFromTarget += dst(oldDistance, oldTheta);
    }

    @Override
    public String toString() {
        return "Probe[" + getName() + "]{" +
                "gravitySpeed=" + gravitySpeed +
                ", gravity acceleration=" + gravityAcceleration +
                ", distance=" + distance +
                ", angularSpeed=" + angularSpeed +
                ", thetaAcceleration=" + thetaAcceleration +
                '}';
    }

    @Override
    public void setGravitySpeed(int gravitySpeed) {
        if (Math.abs(gravitySpeed) > maxGravitySpeed)
            return;
        super.setGravitySpeed(gravitySpeed);
    }


    public char getChar() {
        if (isBuilding())
            return (char) (((char) (3 - progress)) + '0');
        if (isOnBoard())
            return '+';
        return '-';
    }

    private void accelerateGravity(int direction) {
        if (Math.abs(gravityAcceleration) > maxGravityAcceleration)
            return;
        if (direction < 0)
            gravityAcceleration -= stepGravityAcceleration;
        if (direction > 0)
            gravityAcceleration += stepGravityAcceleration;
    }

    private void decelerateGravity(int direction) {
        if (direction > 0)
            gravityAcceleration -= stepGravityAcceleration;
        if (direction < 0)
            gravityAcceleration += stepGravityAcceleration;
    }

    public boolean isFull() {
        return isFull;
    }

    private void setFull(boolean isFull) {
        this.isFull = isFull;
    }

    public int getTurnsSinceLaunch() {
        return turnsSinceLaunch;
    }

    void setTurnsSinceLaunch(int turnsSinceLaunch) {
        this.turnsSinceLaunch = turnsSinceLaunch;
    }

    public void processOutcome(Outcome o) {
        if (o instanceof CrashOutcome) {
            scheduledForRemove = true;

            SoundManager.play(Sounds.CrashSound);
            return;
        }

        this.data.add(o.data);
        this.isFull = true;
        this.turnsSinceLaunch = 0;
        this.setTarget(homeBase);
        this.setGravitySpeed(0);
        this.setAngularSpeed(0f);

    }

    public void drawProjected(Batch batch, float x, float y) {
        projectedSprite.setAlpha(0.5f);
        projectedSprite.setPosition(x, y);
        projectedSprite.draw(batch);

    }

    public void setTarget(OrbitalObject target) {
        this.target = target;
        passedDistanceFromTarget = 0;
        acceleratingGravityDistance = true;
        absoluteLaunchDistance = Math.abs(target.getDistance() - getDistance());
    }

    public OrbitalObject getTarget() {
        return target;
    }

    @Override
    public String getInfo() {
        return getName() +
                "\n" +
                "Target: " + getTarget().getName() + "\n" +
                (isFull() ? "Mission completed" : "") +
                "\n" +
                data.toString();
    }

    @Override
    public ResourceRecord getResourceHolder() {
        return this.data;
    }

    public void dock() {
        setFull(false);
        setOnBoard(true);
        data = new ResourceRecord();
        setAngularSpeed(homeBase.getAngularSpeed());
        setGravitySpeed(0);
    }

    public boolean isBuilding() {
        return building;
    }

    public void setBuilding(boolean building) {
        this.building = building;
    }

    public void kill() {
        scheduledForRemove = true;
    }
}
