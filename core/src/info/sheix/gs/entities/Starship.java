package info.sheix.gs.entities;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import info.sheix.gs.Effects;
import info.sheix.gs.Outcome;
import info.sheix.gs.ResourceRecord;
import info.sheix.gs.util.Messages;
import info.sheix.gs.util.Sounds;
import info.sheix.gs.outcomes.TurnOutcome;
import info.sheix.gs.util.Singleton;
import info.sheix.gs.util.SoundManager;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static info.sheix.gs.Constants.starshipSpeed;
import static info.sheix.gs.entities.Probe.probeStartingSpeed;

public class Starship extends OrbitalObject implements ResourceHolder {

    private int zeroDriveFuel;
    private int zeroDriveFuelToJump;

    public int getFood() {
        return data.food;
    }

    public void setFood(int food) {
        this.data.food = food;
    }

    public int getCrew() {
        return data.crew;
    }

    public void setCrew(int crew) {
        this.data.crew = crew;
    }

    public int getMetal() {
        return data.metal;
    }

    public void setMetal(int metal) {
        this.data.metal = metal;
    }

    public List<Probe> getProbes() {
        probes = probes.stream().filter(p -> !p.scheduledForRemove).collect(Collectors.toList());

        return probes;
    }

    public void setProbes(List<Probe> probes) {
        this.probes = probes;
    }

    public int maxprobes = 5;

    private List<Probe> probes;

    public Starship(Boolean real) {
        setName("Voyager II");
        if (real)
            this.sprite = new Sprite(Singleton.INSTANCE.getAssetManager().get("ship.png", Texture.class));
        zeroDriveFuelToJump = 7;
        setZeroDriveFuel(7);

        distance = 100;
        setTheta(Singleton.INSTANCE.getRandom().nextInt(628) / 100f);
        setAngularSpeed(starshipSpeed);

        data.food = 250;
        data.crew = 10;
        data.metal = 30;
        probes = new ArrayList<>();

    }


    public int getZeroDriveFuel() {
        return zeroDriveFuel;
    }

    public int getZeroDriveFuelToJump() {
        return zeroDriveFuelToJump;
    }


    public void setZeroDriveFuel(int i) {
        zeroDriveFuel = i;

    }

    public String buildProbe() {
        if (probes.size() < maxprobes) {
            if (data.metal >= 10) {
                probes.add(new Probe(this, true));
                data.metal -= 10;
                SoundManager.play(Sounds.BuildProbeInitiated);
                return "Your engineering crew started to build new probe";
            }
            return "There is not enough resources to build a probe, you need 10 metal";

        }
        return "There is no place left for probes in the ship. Launch existing to free space.";

    }

    public Probe launchProbe(OrbitalObject target) {
        for (Probe probe : probes) {
            if (probe.isOnBoard() && !probe.isBuilding()) {
                ResourceRecord launchResource = new ResourceRecord(20, 1, 0);
                launchResource.move(this, probe);

                probe.distance = distance + 5;
                probe.setOnBoard(false);
                probe.setTarget(target);
                probe.setTheta(getTheta());
                setInitialSpeedForProbe(target, probe);

                if ((target.dst(this) > 0)) {
                    probe.distance = distance + 5;
                } else {
                    probe.distance = distance - 5;
                }

                probe.setTurnsSinceLaunch(0);

                return probe;
            }
        }
        return null;
    }

    private void setInitialSpeedForProbe(OrbitalObject target, Probe probe) {
        if (probe.distance - target.distance < 0)
            probe.setGravitySpeed(1);
        else
            probe.setGravitySpeed(-1);

        if (probe.angularDistance(target) > 0)
            probe.angularSpeed = probeStartingSpeed;
        else
            probe.angularSpeed = -probeStartingSpeed;
    }


    public List<TurnOutcome> makeTurn() {
        List<TurnOutcome> turnOutcomes = super.makeTurn();

        if ((getResourceHolder().food == 0) && (getResourceHolder().crew > 0)) {
            turnOutcomes.add(new TurnOutcome(Messages.CREW_MEMBER_DIED, Effects.CREW_MEMBER_DIED));
            getResourceHolder().crew -= 1;
        }

        if (zeroDriveFuel < zeroDriveFuelToJump)
            zeroDriveFuel++;

        return turnOutcomes;
    }


    public boolean isGameOver() {
        return data.crew == 0 && !atLeastOneLaunchedAndMannedProbe();
    }

    private boolean atLeastOneLaunchedAndMannedProbe() {
        for (Probe p : probes) {
            if (!p.isOnBoard() && p.data.crew > 0) {
                return true;
            }
        }
        return false;
    }

    public void dockProbe(Probe probe) {
        this.data.add(probe.data);
        probe.dock();
    }

    @Override
    public void processOutcome(Outcome o) {

    }

    public List<Probe> getAwayProbes() {
        List<Probe> list = new ArrayList<>();
        for (Probe p : probes) {
            if (p.isOnBoard())
                continue;
            list.add(p);
        }
        return list;
    }

    @Override
    public String getInfo() {
        return null;
    }

    @Override
    public ResourceRecord getResourceHolder() {
        return data;
    }

    public boolean haveEnoughFuelToJump() {
        return getZeroDriveFuel() >= getZeroDriveFuelToJump();
    }

    public void setZeroDriveFuelToJump(int zeroDriveFuelToJump) {
        this.zeroDriveFuelToJump = zeroDriveFuelToJump;
    }
}
