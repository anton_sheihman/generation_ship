package info.sheix.gs.entities;

import info.sheix.gs.outcomes.TurnOutcome;

import java.util.List;

/**
 * Created by Sheix on 11/7/2016.
 */
public interface Turnable {
    List<TurnOutcome> makeTurn();
}
