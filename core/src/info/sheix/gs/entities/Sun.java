package info.sheix.gs.entities;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import info.sheix.gs.Outcome;
import info.sheix.gs.util.Singleton;

public class Sun extends OrbitalObject implements SpriteHolder {

    private Sprite sprite;

    public Sun(Sprite sprite) {
        this.sprite = sprite;
        distance = 0;
        angularSpeed = 0;
        setName(generateStarName());
    }

    private int size;

    public int getSize() {
        return size;
    }

    private Color color;

    public void setSize(int i) {
        this.size = i;
    }

    @Override
    public String getInfo() {
        return getName();
    }

    private String generateStarName() {
        return Singleton.INSTANCE.getPlanetsGenerator().generateDifferent(10).stream().filter(s -> s.length() >= 5).findFirst().orElse("Unnamed Star");
    }

    public void setColor(Color randomSunColor) {
        this.color = randomSunColor;

    }

    public Color getSunColor() {
        return color;
    }

    @Override
    public Sprite getSprite() {
        return sprite;
    }

    @Override
    public void processOutcome(Outcome o) {

    }
}
