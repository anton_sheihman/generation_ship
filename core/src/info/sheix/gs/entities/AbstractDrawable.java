package info.sheix.gs.entities;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import info.sheix.gs.Origin;
import info.sheix.gs.util.Singleton;

public abstract class AbstractDrawable implements SpriteHolder {

    protected Sprite sprite;
    private int size;

    @Override
    public Sprite getSprite() {
        return sprite;
    }

    public void draw(Batch batch, float originX, float originY, Origin origin, boolean addOffset) {
        if (getSprite() == null) {
            if (Singleton.INSTANCE.debug)
                System.out.println("sprite is null? " + toString());
            return;
        }
        if (origin == Origin.ZERO)
            getSprite().setOrigin(0, 0);
        else
            getSprite().setOriginCenter();

        float offset = getSprite().getHeight() / 2f;
        if (addOffset)
            getSprite().setPosition(originX - offset, originY - offset);
        else
            getSprite().setPosition(originX, originY);
        getSprite().draw(batch);
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public abstract String getInfo();
}
